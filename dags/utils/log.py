from sys       import exc_info
from logging   import getLogger
from functools import wraps

logger = getLogger('airflow')


def log_and_raise(func):
    @wraps(func)
    def exec_func(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except:
            if exc_info() != (None, None, None):
                logger.error("{} failed args=[{}], kwargs[{}]:".format(func.__name__, args, kwargs), exc_info=True)
            raise
    return exec_func


def log_and_return(func):
    @wraps(func)
    def exec_func(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except:
            if exc_info() != (None, None, None):
                logger.error("{} failed args=[{}], kwargs[{}]:".format(func.__name__, args, kwargs), exc_info=True)
            return False
    return exec_func


def log_and_silence(func):
    @wraps(func)
    def exec_func(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except:
            if exc_info() != (None, None, None):
                logger.error("{} failed args=[{}], kwargs[{}]:".format(func.__name__, args, kwargs), exc_info=True)
    return exec_func
