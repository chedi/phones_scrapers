from bs4   import BeautifulSoup
from types import GeneratorType

from utils.url import url_soup


class SearchHelper(object):
    def __init__(self, url, data=None):
        self.url  = url
        self.soup = BeautifulSoup(data, 'lxml') if data else url_soup(url)

    def mc_find(self, specifications, search_node=None):
        search_node          = search_node if search_node else self.soup
        search_result        = None
        specification        = specifications.pop(0)
        specification_length = len(specification)

        search_callbacks = {
            'f'  : lambda: search_node.find                  ,
            's'  : lambda: search_node.find                  ,
            'a'  : lambda: search_node.findAll               ,
            'l'  : lambda: search_node.findAll               ,
            'c'  : lambda: search_node.findChildren          ,
            'ns' : lambda: search_node.findNextSibling       ,
            'ps' : lambda: search_node.findPreviousSibling   ,
            'pns': lambda: search_node.parent.findNextSibling,
        }

        if specification_length == 3:
            if specification[2] in search_callbacks:
                current_callback = search_callbacks[specification[2]]()
            else:
                raise ValueError('Invalid search type specification')
        else:
            current_callback = search_callbacks['f']()

        if specification_length == 1 or (specification_length in (2, 3) and specification[1] is None):
            search_result = current_callback(specification[0])
        elif specification_length in (2, 3):
            if isinstance(specification[1], dict):
                search_result = current_callback(specification[0], specification[1])
            else:
                search_result = current_callback(specification[0], class_=specification[1])
        else:
            raise ValueError('Can only have 1 to 3 parameter in each search specification')

        if search_result and specification_length == 3:
            if specification[2] == 's':
                search_result = search_result.strings
            elif specification[2] == 'l':
                search_result = search_result[-1]

        if search_result:
            if specifications:
                return self.mc_find(specifications, search_result)
            return search_result

    def find_attr(self, specification, attr, search_node=None):
        result = self.mc_find(specification, search_node)

        def get_attributes(attributes, specifications):
            if not isinstance(specifications, list):
                specifications = [specifications]
            result = {
                specification : attributes.get(specification, None)
                for specification in specifications}
            if len(result) == 1:
                return next(result.itervalues())
            return result

        if result:
            if isinstance(result, list):
                return filter(bool, map(lambda _: get_attributes(_.attrs, attr), result))
            return get_attributes(result.attrs, attr)

    def find_text(self, specification, search_node=None):
        result = self.mc_find(specification, search_node)
        if result:
            if isinstance(result, list) or isinstance(result, GeneratorType):
                return filter(bool, map(lambda _: _.text.strip() if hasattr(_, 'text') else _.strip(), result))
            return result.text.strip()
