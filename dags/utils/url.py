from bs4          import BeautifulSoup
from stem         import Signal
from copy         import deepcopy
from redis        import Redis
from redis        import ConnectionPool
from urllib3      import PoolManager
from urllib3      import disable_warnings
from requesocks   import session
from collections  import deque
from stem.control import Controller

from utils.strings import safe_str


disable_warnings()

redis_conn          = Redis(connection_pool=ConnectionPool(host='localhost', port=6379, db=0))
tor_rebuild_key     = 'tor_rebuid'
tor_rebuild_trigger = 150

redis_conn.set(tor_rebuild_key, 0)

http         = PoolManager(100, timeout=100, retries=5)
http_proxies = deque([
    (port - 1000, session(proxies={'http' : 'socks5://127.0.0.1:{}'.format(port),
                                   'https': 'socks5://127.0.0.1:{}'.format(port)}))
    for port in range(9500, 9601)
])


request_headers = {
    'Accept'    : 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'User-Agent': 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:38.0) Gecko/20100101 Firefox/38.0',
}

class GetResponse(object):
    def __init__(self, url, request, data):
        self.url     = url
        self.data    = data
        self.request = request


def tor_new_id():
    redis_conn.incr(tor_rebuild_key)
    tor_rebuild_requests = int(redis_conn.get(tor_rebuild_key))

    if tor_rebuild_requests > tor_rebuild_trigger:
        redis_conn.set(tor_rebuild_key, 0)
        for port, proxy in http_proxies:
            with Controller.from_port(port=port) as controller:
                controller.authenticate('chikaka')
                controller.signal(Signal.NEWNYM)
    else:
        http_proxies.rotate(1)


def post_url(url, headers, body={}, mut_request=[]):
    while mut_request:
        mut_request.pop()

    mut_request.append(http.request_encode_body ('POST', url, headers=headers, body=body))
    return mut_request[0].data


def url_soup(url, headers={}):
    custom_headers = deepcopy(request_headers)
    custom_headers.update(headers)
    return BeautifulSoup(get_url(url, custom_headers), 'lxml')


def get_cookies(request):
    return {
        'Cookie': '; '.join([
            _.split(';')[0].strip()
            for _ in request[0].getheader('set-cookie').split(',')])}


def get_all_hidden_form_vars(page):
    return {
        safe_str(param['name']) : safe_str(param ['value'])
        for param in page.find_attr(
            [('input', {'type':'hidden'}, 'a')],
            ['name', 'value'])}


def proxy_get_url(url, headers={}):
    for port, proxy in http_proxies:
        try:
            print("{}:{}".format(port, url))
            response = proxy.get(url, headers=headers)
            result   = GetResponse(response.url, response.request, response.text)
            break
        except:
            continue
    else:
        tor_new_id()
        result = GetResponse(url, None, None)
    return result


def get_url(url, headers={}, response=[]):
    result      = http.request('GET', url, headers=headers)
    redirection = result.get_redirect_location()

    response.append(result)

    url = redirection if redirection else url
    return result.data
