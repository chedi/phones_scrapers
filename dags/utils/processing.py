from contextlib      import contextmanager
from multiprocessing import Pool


@contextmanager
def multiprocessing_pool(processes=10):
    pool = Pool(processes=processes)
    yield pool
    pool.close()
    pool.join()
