from base64      import b64decode
from codecs      import getdecoder
from urllib3     import ProxyManager
from itertools   import chain
from collections import deque

from utils.search import SearchHelper


def get_proxy_list_org():
    return list(filter(lambda _: _.lower() != 'proxy', chain.from_iterable([
        SearchHelper("http://proxy-list.org/english/index.php?p={}".format(page_number)).find_text(
            [('li', 'proxy', 'a')]) for page_number in range(1, 11)])))


def get_free_proxy_list_net():
    return list(filter(
        bool, [':'.join(map(lambda _=_: _.text, _.findAll('td')[0:2]))
               for _ in SearchHelper("http://free-proxy-list.net/").mc_find([('tr', None, 'a')])]))


def get_cool_proxy_net():
    rot13 = getdecoder('rot13')

    return list(chain.from_iterable([filter(bool,
        list(map(lambda _: u'{}:{}'.format(str(b64decode(rot13(_[0][1])[0])), _[1][0]) if len(_[0]) == 3 else None,
            filter(bool, [list(map(lambda _=_: _.text.split('"'), _.findAll('td')[0:2]))
                for _ in SearchHelper(url).mc_find([('tr', None, 'a')])])))) for url in [
                    "http://www.cool-proxy.net/proxies/http_proxy_list/sort:score/direction:desc/page:{}".format(page_number)
                    for page_number in range(1, 10)]]))


def get_proxy():
    get_proxy.proxies.rotate(1)
    yield get_proxy.proxies[0]


def refresh_all_proxies():
    get_proxy.proxies = deque(map(lambda _: ProxyManager('http://{}'.format(_)), set(
        get_proxy_list_org() + get_cool_proxy_net() + get_free_proxy_list_net())))
