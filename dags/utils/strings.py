from random import choice
from random import sample
from string import digits
from string import whitespace
from string import punctuation
from string import ascii_letters


def random_spaces_string(length=10):
    return u''.join(choice(whitespace) for _ in xrange(length))


def random_punctuation_string(length=10):
    return u''.join(choice(punctuation) for _ in xrange(length))


def random_letters_string(length=10):
    return u''.join(choice(ascii_letters) for _ in xrange(length))


def random_digits_string(length=10):
    return u''.join(choice(digits) for _ in xrange(length))


def random_string(length=10):
    letters_range     = range(1, length)
    letters_count     = choice(letters_range) if len(letters_range) > 0 else 0
    punctuation_count = length - letters_count
    return ''.join(sample(
        random_letters_string(letters_count) + random_punctuation_string(punctuation_count),
        length
    ))


def random_string_with_spaces(length=10):
    spaces_range  = range(1, length)
    spaces_count  = choice(spaces_range) if len(spaces_range) > 0 else 0
    letters_count = length - spaces_count
    return ''.join(sample(
        random_letters_string(letters_count) + random_spaces_string(spaces_count),
        length
    ))

def safe_unicode(obj, *args):
    try:
        return unicode(obj, *args)
    except UnicodeDecodeError:
        ascii_text = str(obj).encode('string_escape')
        return unicode(ascii_text)

def safe_str(obj):
    try:
        return str(obj)
    except UnicodeEncodeError:
        return unicode(obj).encode('unicode_escape')


def normalize_string(string):
    return safe_str(string.lower().strip())

def normalize_strings(strings):
    return [normalize_string(string) for string in strings]
