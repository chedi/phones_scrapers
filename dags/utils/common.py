def merge_dicts(*args):
    return dict((k, v) for d in args for k, v in d.items())
