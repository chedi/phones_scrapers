# -*- coding: utf-8 -*-
from os      import link
from os      import chown
from os      import chmod
from os      import remove
from os      import system
from pwd     import getpwnam
from grp     import getgrnam
from six     import string_types
from stat    import S_IRUSR
from stat    import S_IRGRP
from os.path import exists

def normalize_price(price):
    if isinstance(price, float) or isinstance(price, int):
        price = str(price)
    if isinstance(price, string_types):
        if price.endswith(u'€'):
            price = price[:-1]
        price = price.replace('.', ',').replace(u'€', ',')
    price = price.strip()

    if price[0] == ',' and price.count(',') > 1:
        price = price[1:]
    if price[-1] == ',':
        price = price[:-1]
    return price

def chown_file(path, username, groupname):
    try:
        uid = getpwnam(username ).pw_uid
        gid = getgrnam(groupname).gr_gid
        chown(path, uid, gid)
    except:
        system('sudo chown {}:{} "{}"'.format(username, groupname, path))


def setup_ftp_file(export_data_file, export_history_file, ftp_user, ftp_group):
    if exists(export_data_file):
        remove(export_data_file)
    chmod(export_history_file, S_IRUSR | S_IRGRP)
    link(export_history_file, export_data_file)
    chown_file(export_data_file   , ftp_user, ftp_group)
    chown_file(export_history_file, ftp_user, ftp_group)
