class NavigationStep(object):
    def __init__(self, url, name, option, value, previous_step, section_node=None):
        self.url           = url
        self.name          = name
        self.value         = value
        self.option        = option
        self.next_steps    = list()
        self.section_node  = section_node
        self.previous_step = previous_step

        if self.previous_step:
            self.previous_step.next_steps.append(self)

    def __str__(self):
        return self.name

    def get_all_paths(self):
        iterator  = self
        all_paths = []

        while iterator.previous_step:
            iterator = iterator.previous_step

        def get_sub_paths(step, paths):
            if step.next_steps:
                for sub_step in step.next_steps:
                    get_sub_paths(sub_step, paths)
            else:
                results = []
                while step.previous_step:
                    results.append(step)
                    step = step.previous_step
                results.reverse()
                paths.append(results)
        get_sub_paths(iterator, all_paths)
        return all_paths

    def get_previous_options(self):
        iterator         = self
        previous_options = {}

        while iterator.previous_step:
            iterator = iterator.previous_step
            previous_options.update({iterator.option: iterator.name})
        return previous_options


class SectionNode(object):
    def __init__(self, current_node, node_type, next_node=None):
        self.next_node    = next_node
        self.node_type    = node_type
        self.current_node = current_node

    @property
    def next_node_name(self):
        if self.next_node:
            return self.next_node.current_node

    @property
    def current_node_name(self):
        return self.current_node
