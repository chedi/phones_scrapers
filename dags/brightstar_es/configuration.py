from redis import Redis
from redis import ConnectionPool

from common.configuration import get_or_create
from common.configuration import DEFAULT_CONFIG

redis_conn        = Redis(connection_pool=ConnectionPool(host='localhost', port=6379, db=0))
post_headers      = {'Content-Type': 'application/x-www-form-urlencoded'}
site_step_2_url   = 'https://appleonlineesp.mpxltd.co.uk/inspection.aspx'
products_tree_key = 'brightstar_es_products_tree'
products_list_key = 'brightstar_es_products_list'

ftp_user             = get_or_create('brightstar_es_ftp_user'            , DEFAULT_CONFIG['RECOMERCE_FTP_USER'     ])
ftp_group            = get_or_create('brightstar_es_ftp_group'           , DEFAULT_CONFIG['RECOMERCE_FTP_GROUP'    ])
ftp_data_path        = get_or_create('brightstar_es_ftp_data_path'       , DEFAULT_CONFIG['RECOMERCE_FTP_DATA_PATH'])
processing_pool_size = get_or_create('brightstar_es_processing_pool_size', DEFAULT_CONFIG['PROCESSING_POOL_SIZE'   ])

ftp_history_path = get_or_create('brightstar_es_ftp_history_path', DEFAULT_CONFIG['RECOMERCE_FTP_HYSTORY_PATH'] + '/brightstar_es')
