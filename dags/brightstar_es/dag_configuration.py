from datetime          import time
from datetime          import datetime
from datetime          import timedelta
from airflow.models    import DAG
from airflow.operators import PythonOperator

from common.configuration  import get_or_create
from brightstar_es.scraper import export_data
from brightstar_es.scraper import scrap_products
from brightstar_es.scraper import clear_redis_keys
from brightstar_es.scraper import explore_site_tree

dag = DAG(dag_id='scraper_brightstar_es', default_args={
    'owner'            : 'airflow',
    'email'            : get_or_create('ADMINS_EMAILS'),
    'start_date'       : datetime.combine(datetime.today() - timedelta(1), time(1)),
    'email_on_retry'   : True,
    'email_on_failure' : True,
    'schedule_interval': timedelta(1)})

export_data_task       = PythonOperator(dag=dag, task_id='export_data'      , python_callable=export_data      )
scrap_products_task    = PythonOperator(dag=dag, task_id='scrap_products'   , python_callable=scrap_products   )
clear_redis_keys_task  = PythonOperator(dag=dag, task_id='clear_redis_keys' , python_callable=clear_redis_keys )
explore_site_tree_task = PythonOperator(dag=dag, task_id='explore_site_tree', python_callable=explore_site_tree)

export_data_task      .set_upstream(scrap_products_task   )
scrap_products_task   .set_upstream(explore_site_tree_task)
explore_site_tree_task.set_upstream(clear_redis_keys_task )
