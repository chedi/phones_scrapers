from datetime          import time
from datetime          import datetime
from datetime          import timedelta
from airflow.models    import DAG
from airflow.operators import PythonOperator
from airflow.operators import EmailOperator
from airflow.operators import BranchPythonOperator

from vendre_ch.scraper    import export_data
from vendre_ch.scraper    import alert_options
from vendre_ch.scraper    import scrap_products
from vendre_ch.scraper    import clear_redis_keys
from vendre_ch.scraper    import explore_site_tree
from vendre_ch.scraper    import calculator_checking
from common.configuration import get_or_create

dag = DAG(dag_id='scraper_vendre_ch', default_args={
    'owner'            : 'airflow',
    'email'            : get_or_create('ADMINS_EMAILS'),
    'start_date'       : datetime.combine(datetime.today() - timedelta(1), time(1)),
    'email_on_retry'   : True,
    'email_on_failure' : True,
    'schedule_interval': timedelta(1)})

alert_task               = EmailOperator       (dag=dag, task_id='alert'              , **alert_options()                  )
export_data_task         = PythonOperator      (dag=dag, task_id='export_data'        , python_callable=export_data        )
scrap_products_task      = PythonOperator      (dag=dag, task_id='scrap_products'     , python_callable=scrap_products     )
clear_redis_keys_task    = PythonOperator      (dag=dag, task_id='clear_redis_keys'   , python_callable=clear_redis_keys   )
explore_site_tree_task   = PythonOperator      (dag=dag, task_id='explore_site_tree'  , python_callable=explore_site_tree  )
calculator_checking_task = BranchPythonOperator(dag=dag, task_id='calculator_checking', python_callable=calculator_checking)

alert_task              .set_upstream(calculator_checking_task)
export_data_task        .set_upstream(scrap_products_task     )
scrap_products_task     .set_upstream(calculator_checking_task)
explore_site_tree_task  .set_upstream(clear_redis_keys_task   )
calculator_checking_task.set_upstream(explore_site_tree_task  )
