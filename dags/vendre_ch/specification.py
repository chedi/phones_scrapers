from collections import OrderedDict

from vendre_ch.structures import VariationCriteria
from vendre_ch.structures import GradeSpecification


MODELS_VARIATIONS_CRITERIA = [
    [VariationCriteria('Memoire', u"M\xe9moire vive")],
    [VariationCriteria('Memoire', u"Quelle est la capacit\xe9/m\xe9moire de votre appareil ?"),
     VariationCriteria('Disque', u"M\xe9moire de la disque dur")]]

GRADES = OrderedDict([
    ('grade A', [
        GradeSpecification(u"Dans quel \xe9tat est le bord?"                         , u"Aucune trace d\u2019usure"),
        GradeSpecification(u"Dans quel \xe9tat est la face avant?"                   , u"Aucune trace d\u2019usure"),
        GradeSpecification(u"L'appareil fonctionne-t-il parfaitement?"               , u"Oui"),
        GradeSpecification(u'Quel est l\u2019\xe9tat de l\u2019appareil?'            , u"Pas de traces d'utilisation"),
        GradeSpecification(u"Dans quel \xe9tat est la face arri\xe8re de l'appareil?", u"Aucune trace d\u2019usure")]),
    ('grade B', [
        GradeSpecification(u"Dans quel \xe9tat est le bord?"                         , u"Traces d\u2019usure visibles"),
        GradeSpecification(u"Dans quel \xe9tat est la face avant?"                   , u"Traces d\u2019usure visibles"),
        GradeSpecification(u"L'appareil fonctionne-t-il parfaitement?"               , u"Oui"),
        GradeSpecification(u'Quel est l\u2019\xe9tat de l\u2019appareil?'            , u"Traces d'utilisation visibles"),
        GradeSpecification(u"Dans quel \xe9tat est la face arri\xe8re de l'appareil?", u"Traces d\u2019usure visibles")]),
    ('grade C', [
        GradeSpecification(u"Dans quel \xe9tat est le bord?"                         , u"Traces d\u2019usure palpables"),
        GradeSpecification(u"Dans quel \xe9tat est la face avant?"                   , u"Traces d\u2019usure palpables"),
        GradeSpecification(u"L'appareil fonctionne-t-il parfaitement?"               , u"Oui"),
        GradeSpecification(u'Quel est l\u2019\xe9tat de l\u2019appareil?'            , u"Traces d'utilisation palpables"),
        GradeSpecification(u"Dans quel \xe9tat est la face arri\xe8re de l'appareil?", u"Traces d\u2019usure palpables")]),
    ('grade D', [
        GradeSpecification(u"Dans quel \xe9tat est le bord?"                         , u"Brisures ou cassures"),
        GradeSpecification(u"Dans quel \xe9tat est la face avant?"                   , u"Brisures ou cassures"),
        GradeSpecification(u"L'appareil fonctionne-t-il parfaitement?"               , u"Oui"),
        GradeSpecification(u'Quel est l\u2019\xe9tat de l\u2019appareil?'            , u"Traces d'utilisation"),
        GradeSpecification(u"Dans quel \xe9tat est la face arri\xe8re de l'appareil?", u"Brisures ou cassures")])
    ])
