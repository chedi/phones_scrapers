from redis import Redis
from redis import ConnectionPool

from common.configuration import get_or_create
from common.configuration import DEFAULT_CONFIG

redis_conn          = Redis(connection_pool=ConnectionPool(host='localhost', port=6379, db=0))
base_site_url       = 'http://www.vendre.ch'
products_list_key   = 'vendre_ch_products'
categories_list_key = 'vendre_ch_categories'

ftp_user             = get_or_create('vendre_ch_ftp_user'            , DEFAULT_CONFIG['RECOMERCE_FTP_USER'        ])
ftp_group            = get_or_create('vendre_ch_ftp_group'           , DEFAULT_CONFIG['RECOMERCE_FTP_GROUP'       ])
ftp_data_path        = get_or_create('vendre_ch_ftp_data_path'       , DEFAULT_CONFIG['RECOMERCE_FTP_DATA_PATH'   ])
processing_pool_size = get_or_create('vendre_ch_processing_pool_size', DEFAULT_CONFIG['PROCESSING_POOL_SIZE'      ])

ftp_history_path = get_or_create('vendre_ch_ftp_history_path', DEFAULT_CONFIG['RECOMERCE_FTP_HYSTORY_PATH'] + '/vendre_ch')
