from csv               import DictWriter
from pickle            import dumps as pdumps
from pickle            import loads as ploads
from os.path           import join
from datetime          import datetime

from utils.url                      import get_url
from utils.log                      import log_and_silence
from utils.search                   import SearchHelper
from utils.export                   import setup_ftp_file
from utils.export                   import normalize_price
from utils.strings                  import safe_str
from utils.processing               import multiprocessing_pool
from mobilerecycle_ch.export        import CSV_DEFAULT_DATA
from mobilerecycle_ch.export        import CSV_DATA_EXPORT_HEADERS
from mobilerecycle_ch.structures    import ModelPrices
from mobilerecycle_ch.structures    import BrandInformation
from mobilerecycle_ch.structures    import ProductSpecification
from mobilerecycle_ch.configuration import redis_conn
from mobilerecycle_ch.configuration import memory_regex
from mobilerecycle_ch.configuration import base_site_url
from mobilerecycle_ch.configuration import price_headers
from mobilerecycle_ch.configuration import brands_list_key
from mobilerecycle_ch.configuration import price_rest_call
from mobilerecycle_ch.configuration import processing_pool_size
from mobilerecycle_ch.configuration import ftp_user
from mobilerecycle_ch.configuration import ftp_group
from mobilerecycle_ch.configuration import ftp_data_path
from mobilerecycle_ch.configuration import ftp_history_path


def clear_redis_keys():
    for brand_key in redis_conn.hgetall(brands_list_key):
        redis_conn.delete(brand_key)
    redis_conn.delete(brands_list_key)

@log_and_silence
def explore_site_tree_worker(brand_option):
    url               = brand_option.attrs['value']
    label             = ' '.join(brand_option.text.replace('\n', '').split()).strip()
    hack_url          = base_site_url + url + '?page=1000'
    last_page_url     = SearchHelper(hack_url).find_attr([('div', 'pagination'), ('a', None, 'a')], 'href')
    last_page_number  = int(last_page_url[-1][len(url + '?page='):]) if last_page_url else 1
    brand_information = BrandInformation(label, url, last_page_number)
    redis_conn.hset(brands_list_key, base_site_url + url, pdumps(brand_information))

def explore_site_tree():
    brands_options = SearchHelper(base_site_url).mc_find([('select', {'data-type':'hash'}), ('option', None, 'a')])
    with multiprocessing_pool(processes=processing_pool_size) as pool:
        for brand_option in brands_options:
            if brand_option.attrs['value'] != '#':
                pool.apply_async(explore_site_tree_worker, (brand_option,))

@log_and_silence
def model_worker(model_url, brand_url):
    url           = base_site_url + model_url
    model         = SearchHelper(url)
    label         = model.find_text([('div', 'handset-resume'), ('span', 'description')]).split('\n')[-1]
    label         = ' '.join(label.replace('\n', '').split()).strip()
    model_id      = model.find_attr([('form', {'data-source':'/transaction/calculcate-price'})], 'data-model')
    model_prices  = ModelPrices(
        broken_price  = get_url(price_rest_call.format(model_id, 0), price_headers).split('|')[0],
        working_price = get_url(price_rest_call.format(model_id, 1), price_headers).split('|')[0])
    redis_conn.hset(brand_url, url, pdumps(ProductSpecification(label, model_url, model_prices)))

def scrap_products():
    with multiprocessing_pool(processes=processing_pool_size) as pool:
        for brand_url, raw_brand in redis_conn.hgetall(brands_list_key).iteritems():
            brand = ploads(raw_brand)
            for page_number in range(1, brand.pages + 1):
                page        = SearchHelper('{}?page={}'.format(brand_url, page_number))
                models_urls = page.find_attr([('a', 'handset', 'a')], 'href')

                if models_urls:
                    for model_url in models_urls:
                        pool.apply_async(model_worker, (model_url, brand_url))

def export_data():
    data = redis_conn.hgetall(brands_list_key)
    if data:
        export_data_file    = join(ftp_data_path, 'mobilerecycle_ch.csv')
        export_history_file = join(ftp_history_path, 'mobilerecycle_{}.csv'.format(datetime.now()))

        with open(export_history_file, 'w') as csv_file:
            writer = DictWriter(csv_file, fieldnames=CSV_DATA_EXPORT_HEADERS, delimiter=';')
            writer.writeheader()
            for brand_url, raw_brand in data.iteritems():
                brand = ploads(raw_brand)
                for product_url, raw_product in redis_conn.hgetall(brand_url).iteritems():
                    product  = ploads(raw_product)
                    row_data = CSV_DEFAULT_DATA.copy()
                    for price, grade in [(product.price.working_price, 'Grade A'), (product.price.broken_price, 'Grade B')]:
                        memory_match = memory_regex.match(product.label)
                        memory_data  = memory_match.group(1) if memory_match else ''

                        row_data.update({
                            'Url'      : product_url,
                            'Name'     : safe_str(brand.label + ' ' + product.label),
                            'Brand'    : safe_str(brand.label),
                            'Model'    : safe_str(product.label),
                            'Price'    : normalize_price(price),
                            'Grading'  : grade,
                            'Storage'  : memory_data,
                            'Timestamp': product.created,
                        })
                        writer.writerow(row_data)
        setup_ftp_file(export_data_file, export_history_file, ftp_user, ftp_group)
