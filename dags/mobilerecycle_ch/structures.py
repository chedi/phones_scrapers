from datetime    import datetime
from collections import namedtuple

ModelPrices = namedtuple('ModelPrices', ['working_price', 'broken_price'])

class ProductSpecification(namedtuple('Product', ['label', 'url', 'price', 'created'])):
    def __new__(cls, label, url, price, created=datetime.now()):
        return super(ProductSpecification, cls).__new__(cls, label, url, price, created)


class BrandInformation(namedtuple('BrandInformation', ['label', 'url', 'pages'])):
    def __new__(cls, label, url, pages=1, created=datetime.now()):
        return super(BrandInformation, cls).__new__(cls, label, url, pages)
