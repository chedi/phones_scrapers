from re                   import compile
from redis                import Redis
from redis                import ConnectionPool
from common.configuration import get_or_create
from common.configuration import DEFAULT_CONFIG

redis_conn      = Redis(connection_pool=ConnectionPool(host='localhost', port=6379, db=0))
memory_regex    = compile('.+\s+(\d+[gG]{1}[bBoO]{1})\s*$')
base_site_url   = 'http://mobilerecycle.ch'
price_headers   = {'X-Requested-With': 'XMLHttpRequest'}
brands_list_key = 'mobilerecycle_ch_brands'
price_rest_call = 'http://mobilerecycle.ch/transaction/calculcate-price?model={}&prices=handsetStatus:{},locked:0'

ftp_user             = get_or_create('mobilerecycle_ftp_user'            , DEFAULT_CONFIG['RECOMERCE_FTP_USER'        ])
ftp_group            = get_or_create('mobilerecycle_ftp_group'           , DEFAULT_CONFIG['RECOMERCE_FTP_GROUP'       ])
ftp_data_path        = get_or_create('mobilerecycle_ftp_data_path'       , DEFAULT_CONFIG['RECOMERCE_FTP_DATA_PATH'   ])
processing_pool_size = get_or_create('mobilerecycle_processing_pool_size', DEFAULT_CONFIG['PROCESSING_POOL_SIZE'      ])

ftp_history_path = get_or_create('mobilerecycle_ftp_history_path', DEFAULT_CONFIG['RECOMERCE_FTP_HYSTORY_PATH'] + '/mobilerecycle')
