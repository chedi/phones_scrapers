from __future__ import division

from pickle     import dumps as pdumps
from pickle     import loads as ploads
from os.path    import join
from datetime   import datetime
from unicodecsv import DictWriter

from utils.export                  import normalize_price
from utils.export                  import setup_ftp_file
from utils.search                  import SearchHelper
from utils.processing              import multiprocessing_pool
from vendremonmobile.export        import CSV_DEFAULT_DATA
from vendremonmobile.export        import CSV_DATA_EXPORT_HEADERS
from vendremonmobile.configuration import ftp_user
from vendremonmobile.configuration import ftp_group
from vendremonmobile.configuration import start_url
from vendremonmobile.configuration import redis_conn
from vendremonmobile.configuration import ftp_data_path
from vendremonmobile.configuration import base_site_url
from vendremonmobile.configuration import ftp_history_path
from vendremonmobile.configuration import products_list_key
from vendremonmobile.configuration import products_tree_key
from vendremonmobile.configuration import processing_pool_size

def clear_redis_keys():
    for product_key in redis_conn.hgetall(products_tree_key):
        redis_conn.delete(product_key)
    for product_key in redis_conn.hgetall(products_list_key):
        redis_conn.delete(product_key)
    redis_conn.delete(products_tree_key)
    redis_conn.delete(products_list_key)

def explore_site_tree_worker(product_page):
    url        = base_site_url + product_page
    page       = SearchHelper(url)
    brand_name = product_page.split('/')[-1]

    redis_conn.hset(products_tree_key, url, pdumps(
        (brand_name, filter(bool, filter(lambda _: _.startswith('/recyclage/'),
            page.find_attr([('div', 'main-content'), ('a', None, 'a')], 'href'))))))

def explore_site_tree():
    page = SearchHelper(start_url)

    for phones_page in page.find_attr([('table', 'tous_les_mobiles'), ('a', None, 'a')], 'href'):
        explore_site_tree_worker(phones_page)

def extract_offer_info(offer):
    tds = offer.findAll('td')

    shipping_logo     = tds[3].find('img').attrs['alt']
    functional_prices = map(
        lambda _: normalize_price(_.split()[-1]),
        filter(bool, map(lambda _: _.text, tds[1].findAll('a'))))

    return {
        'shipping'            : 0 if shipping_logo == 'vendre-recyclage-Livraison gratuite' else 'payant',
        'timestamp'           : datetime.now(),
        'seller_name'         : tds[0].find('a').attrs['href'].replace('/', ''),
        'as_new_price'        : functional_prices[1] if len(functional_prices) > 1 else None,
        'functional_price'    : functional_prices[0],
        'non_functional_price': normalize_price(tds[2].find('a').text)}

def scrap_products_worker(brand_name, product_url):
    url            = (base_site_url + product_url).replace(' ', '%20')
    page           = SearchHelper(url)
    product_offers = page.mc_find([('table', 'row', 'a')])

    if product_offers:
        redis_conn.hset(products_list_key, url, pdumps({
            'url'            : url,
            'brand_name'     : brand_name,
            'product_name'   : product_url.split('/')[-1],
            'products_offers': filter(bool, map(extract_offer_info, product_offers))
        }))

def scrap_products():
    with multiprocessing_pool(processes=processing_pool_size) as pool:
        for page_url, raw_products_urls in redis_conn.hgetall(products_tree_key).iteritems():
            brand_name, product_urls = ploads(raw_products_urls)
            for product_url in product_urls:
                pool.apply_async(scrap_products_worker, (brand_name, product_url))

def export_data():
    data = redis_conn.hgetall(products_list_key)
    if data:
        export_data_file    = join(ftp_data_path, 'vendremonmobile.csv')
        export_history_file = join(ftp_history_path, 'vendremonmobile_{}.csv'.format(datetime.now()))

        with open(export_history_file, 'w') as csv_file:
            writer = DictWriter(csv_file, fieldnames=CSV_DATA_EXPORT_HEADERS, delimiter=';')
            writer.writeheader()
            for url, raw_product in data.iteritems():
                row_data     = CSV_DEFAULT_DATA.copy()
                product_data = ploads(raw_product)

                for product_offer in product_data['products_offers']:
                    if product_offer:
                        row_data.update({
                            'Url'           : product_data ['url'             ],
                            'Name'          : product_data ['product_name'    ],
                            'Brand'         : product_data ['brand_name'      ],
                            'Price'         : product_offer['functional_price'],
                            'Seller'        : product_offer['seller_name'     ],
                            'Grading'       : 'Fonctionnel',
                            'Timestamp'     : product_offer['timestamp'],
                            'Delivery_price': product_offer['shipping' ],
                        })
                        writer.writerow(row_data)
                        row_data.update({
                            'Price'  : product_offer['non_functional_price'],
                            'Grading': 'Non fonctionnel'
                        })
                        writer.writerow(row_data)

                        if product_offer['as_new_price']:
                            row_data.update({
                                'Price'  : product_offer['as_new_price'],
                                'Grading': 'Comme neuf'
                            })
                            writer.writerow(row_data)
        setup_ftp_file(export_data_file, export_history_file, ftp_user, ftp_group)
