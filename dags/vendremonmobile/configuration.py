from redis import Redis
from redis import ConnectionPool

from common.configuration import get_or_create
from common.configuration import DEFAULT_CONFIG

redis_conn         = Redis(connection_pool=ConnectionPool(host='localhost', port=6379, db=0))
post_headers       = {'Content-Type': 'application/x-www-form-urlencoded'}
start_url          = 'http://www.vendremonmobile.com/rachat-tous-les-mobiles'
base_site_url      = 'http://www.vendremonmobile.com'
products_tree_key  = 'vendre_mon_mobile_products_tree'
products_list_key  = 'vendre_mon_mobile_products_list'

ftp_user             = get_or_create('vendre_mon_mobile_ftp_user'            , DEFAULT_CONFIG['RECOMERCE_FTP_USER'     ])
ftp_group            = get_or_create('vendre_mon_mobile_ftp_group'           , DEFAULT_CONFIG['RECOMERCE_FTP_GROUP'    ])
ftp_data_path        = get_or_create('vendre_mon_mobile_ftp_data_path'       , DEFAULT_CONFIG['RECOMERCE_FTP_DATA_PATH'])
processing_pool_size = get_or_create('vendre_mon_mobile_processing_pool_size', DEFAULT_CONFIG['PROCESSING_POOL_SIZE'   ])

ftp_history_path = get_or_create('vendre_mon_mobile_ftp_history_path', DEFAULT_CONFIG['RECOMERCE_FTP_HYSTORY_PATH'] + '/vendre_mon_mobile')
