from __future__ import division

from pickle     import loads as ploads
from pickle     import dumps as pdumps
from os.path    import join
from datetime   import datetime
from itertools  import izip
from unicodecsv import DictWriter

from utils.url     import tor_new_id
from utils.url     import proxy_get_url
from utils.export  import normalize_price
from utils.export  import setup_ftp_file
from utils.search  import SearchHelper
from export        import CSV_DEFAULT_DATA
from export        import CSV_DATA_EXPORT_HEADERS
from configuration import ftp_user
from configuration import ftp_group
from configuration import redis_conn
from configuration import get_headers
from configuration import category_id
from configuration import section_name
from configuration import ftp_data_path
from configuration import base_site_url
from configuration import site_start_url
from configuration import ftp_history_path
from configuration import products_list_key
from configuration import products_tree_key
from configuration import export_data_file_name
from configuration import export_history_file_name


def clear_redis_keys():
    for product_key in redis_conn.hgetall(products_tree_key):
        redis_conn.delete(product_key)
    redis_conn.delete(products_tree_key)
    for product_key in redis_conn.hgetall(products_list_key):
        redis_conn.delete(product_key)
    redis_conn.delete(products_tree_key)
    redis_conn.delete(products_list_key)


def explore_site_tree_worker(url, brand_name, brand_id):
    forged_url = url + '&c={}&m1={}&m2=*'.format(category_id, brand_id)

    while True:
        models_data = SearchHelper(
            forged_url, proxy_get_url(forged_url, headers=get_headers).data).mc_find(
                [('div', 'dropbox_two'), ('option', None, 'a')])

        if models_data is not None:
            break
        else:
            tor_new_id()

    for model_data in models_data:
        model_id   = model_data.attrs['value']
        model_name = model_data.text

        if model_id:
            redis_conn.hset(products_tree_key, u'{}-{}'.format(brand_name, model_name), pdumps({
                'brand_id'  : brand_id  ,
                'model_id'  : model_id  ,
                'brand_name': brand_name,
                'model_name': model_name,
            }))


def explore_site_tree():
    while True:
        sections = {
            _.text: _.attrs['href']
            for _ in SearchHelper(site_start_url,
                                  proxy_get_url(site_start_url, headers=get_headers).data
            ).mc_find([('div', {'id': 'container'}),('div', 'inner'), ('a', None, 'a')])}

        base_url = base_site_url + sections[section_name]
        get_headers.update({'Referer': base_url.split('&')[0]})

        brands_data = SearchHelper(
            base_url, proxy_get_url(base_url, headers=get_headers).data).mc_find(
                [('div', 'dropbox_one'), ('option', None, 'a')])

        if brands_data is not None:
            break
        else:
            tor_new_id()

    for brand_name, brand_id in [(_.text, _.attrs['value'])
                                 for _ in brands_data if _.attrs['value']]:
        explore_site_tree_worker(base_url, brand_name, brand_id)


def scrap_products_worker(product):
    while True:
        page = SearchHelper(site_start_url, proxy_get_url(site_start_url, headers=get_headers).data)

        if page:
            elements = page.mc_find([('div', {'id': 'container'}),('div', 'inner'), ('a', None, 'a')])
        else:
            continue

        if elements:
            sections = {_.text: _.attrs['href'] for _ in elements}
        else:
            continue

        product_url = '{}{}&m1={}&m2={}'.format(
            base_site_url, sections[section_name], product['brand_id'], product['model_id'])
        print(product_url)

        prices_page = SearchHelper(product_url, proxy_get_url(product_url, headers=get_headers).data)
        if prices_page:
            prices_data = prices_page.find_attr([('div', {'id':'prices'}), ('input', None, 'a')], 'value')
            if prices_data is not None:
                break
            else:
                tor_new_id()

    products_iter = iter(prices_data)

    for grade_name, price in izip(products_iter, products_iter):
        redis_conn.hset(products_list_key, u'{}-{}-{}'.format(
            product['brand_id'], product['model_id'], grade_name),
                pdumps({
                    'Url'      : product_url,
                    'Name'     : u'{} {}'.format(product['brand_name'], product['model_name']),
                    'Brand'    : product['brand_name'],
                    'Model'    : product['model_name'],
                    'Price'    : normalize_price(price),
                    'Grading'  : grade_name,
                    'Timestamp': datetime.now()
                }))


def scrap_products():
    for raw_product in redis_conn.hgetall(products_tree_key).itervalues():
        scrap_products_worker(ploads(raw_product))


def export_data():
    data = redis_conn.hgetall(products_list_key)
    if data:
        export_data_file    = join(ftp_data_path   , export_data_file_name)
        export_history_file = join(ftp_history_path, export_history_file_name.format(datetime.now()))

        with open(export_history_file, 'w') as csv_file:
            writer = DictWriter(csv_file, fieldnames=CSV_DATA_EXPORT_HEADERS, delimiter=';')
            writer.writeheader()
            for url, raw_product in data.iteritems():
                row_data     = CSV_DEFAULT_DATA.copy()
                product_data = ploads(raw_product)

                row_data.update(product_data)
                writer.writerow(row_data)
        setup_ftp_file(export_data_file, export_history_file, ftp_user, ftp_group)
