from __future__ import division

from math            import ceil
from pickle          import dumps as pdumps
from pickle          import loads as ploads
from os.path         import join
from datetime        import datetime
from unicodecsv      import DictWriter

from utils.export                import normalize_price
from utils.export                import setup_ftp_file
from utils.search                import SearchHelper
from utils.processing            import multiprocessing_pool
from priceminister.export        import CSV_DEFAULT_DATA
from priceminister.export        import CSV_DATA_EXPORT_HEADERS
from priceminister.configuration import ftp_user
from priceminister.configuration import ftp_group
from priceminister.configuration import redis_conn
from priceminister.configuration import ftp_data_path
from priceminister.configuration import base_site_url
from priceminister.configuration import cid_pid_regex
from priceminister.configuration import ftp_history_path
from priceminister.configuration import phones_start_url
from priceminister.configuration import product_info_url
from priceminister.configuration import tablets_start_url
from priceminister.configuration import products_list_key
from priceminister.configuration import products_tree_key
from priceminister.configuration import offer_sales_regex
from priceminister.configuration import product_url_regex
from priceminister.configuration import processing_pool_size

def clear_redis_keys():
    for product_key in redis_conn.hgetall(products_tree_key):
        redis_conn.delete(product_key)
    redis_conn.delete(products_tree_key)
    for product_key in redis_conn.hgetall(products_list_key):
        redis_conn.delete(product_key)
    redis_conn.delete(products_tree_key)
    redis_conn.delete(products_list_key)

def get_products_info(page):
    return (int(page.find_text(
        [('div', {'class':'navFilter'}), ('span',)]).split(' ')
        [0].encode('ascii', 'ignore')),
        len(page.mc_find([('div', {'class': 'navItem'}, 'a')])))

def explore_site_tree_worker(base_url, product_page):
    url  = '{}/pa/{}'.format(base_url, product_page)
    page = SearchHelper(url)
    redis_conn.hset(products_tree_key, url, pdumps([
        product.find('a').attrs['href']
        for product in page.mc_find([('div', {'class': 'navItem'}, 'a')])]))

def explore_site_tree():
    phones_count , phones_per_page  = get_products_info(SearchHelper(phones_start_url ))
    tablets_count, tablets_per_page = get_products_info(SearchHelper(tablets_start_url))

    with multiprocessing_pool(processes=processing_pool_size) as pool:
        for phones_page in range(1, int(ceil(phones_count/phones_per_page)) + 1):
            pool.apply_async(explore_site_tree_worker, (phones_start_url, phones_page))
        for tablets_page in range(1, int(ceil(tablets_count/tablets_per_page)) + 1):
            pool.apply_async(explore_site_tree_worker, (tablets_start_url, tablets_page))

def extract_offer_info(offer):
    if len(offer.findAll('td')) == 4:
        comments                = offer.find(class_='sellerComment')
        seller_rating_container = offer.find(class_='sellerRating')

        if seller_rating_container:
            saller_sales  = offer_sales_regex.search(seller_rating_container.find(class_='sales').text).group(1)
            saller_rating = seller_rating_container.find(class_='value').text
        else:
            saller_sales  = 0
            saller_rating = ''
        return {
            'price'        : offer.find(class_='price').text.split()[0],
            'status'       : offer.find(class_='advertType').text.strip(),
            'shipping'     : offer.find(class_='shipping').text.strip(),
            'comments'     : comments.text.strip() if comments else '',
            'timestamp'    : datetime.now(),
            'pro_seller'   : '(PRO) ' if offer.find(class_='proSeller') else '(NO PRO) ',
            'seller_name'  : offer.find(class_='sellerName').text.strip(),
            'seller_sales' : saller_sales,
            'seller_rating': saller_rating}

def scrap_products_worker(product_url):
    page = details = SearchHelper(base_site_url + product_url + '&all=true')

    if not page.mc_find([('div', 'fpProduct')]):
        for script in filter(lambda _: _.string is not None, page.mc_find([('script', None, 'a')])):
            search_result = product_url_regex.search(script.string)
            if search_result:
                page    = SearchHelper(base_site_url + search_result.group(1) + '&all=true')
                details = SearchHelper(product_info_url.format(*cid_pid_regex.match(product_url).groups()))
                break
        else:
            raise Exception('failed to get the product information url')


    product_offers_container  = page.mc_find([('div', {'itemprop':'offers'}), ('tbody',), ('tr', None, 'a')])
    product_details_container = details.mc_find([('div', 'specs_ctn'), ('tr', None, 'a')])

    redis_conn.hset(products_list_key, product_url, pdumps({
        'url'            : product_url,
        'rating'         : page.find_text([('div', 'productTitle'), ('span', {'itemprop': 'ratingValue'})]),
        'brand_name'     : page.find_text([('div', 'productTitle'), ('span', 'hbis')]),
        'product_name'   : page.find_text([('div', 'productTitle'), ('h1', {'itemprop': 'name'})]),
        'product_details': {_.find(class_='label').text: _.find(class_='value').text
            for _ in product_details_container} if product_details_container else {},
        'products_offers': filter(bool, [extract_offer_info(offer)
            for offer in product_offers_container]) if product_offers_container else []
    }))

def scrap_products():
    with multiprocessing_pool(processes=processing_pool_size) as pool:
        for page_url, raw_products_urls in redis_conn.hgetall(products_tree_key).iteritems():
            for product_url in ploads(raw_products_urls):
                pool.apply_async(scrap_products_worker, (product_url,))

def export_data():
    data = redis_conn.hgetall(products_list_key)
    if data:
        export_data_file    = join(ftp_data_path, 'priceminister.csv')
        export_history_file = join(ftp_history_path, 'priceminister_{}.csv'.format(datetime.now()))

        with open(export_history_file, 'w') as csv_file:
            writer = DictWriter(csv_file, fieldnames=CSV_DATA_EXPORT_HEADERS, delimiter=';')
            writer.writeheader()
            for url, raw_product in data.iteritems():
                row_data      = CSV_DEFAULT_DATA.copy()
                product_data  = ploads(raw_product)
                product_color = ' - '.join([
                    u'{}: {}'.format(k, v) for k, v in product_data['product_details'].iteritems()
                    if k.lower().strip().startswith('couleur')])
                product_storage = ' - '.join([
                    u'{}: {}'.format(k, v) for k, v in product_data['product_details'].iteritems()
                    if k.lower().strip().startswith('stockage')])

                for product_offer in product_data['products_offers']:
                    if product_offer:
                        row_data.update({
                            'Url'               : product_data['url'         ],
                            'Name'              : product_data['product_name'],
                            'Brand'             : product_data['brand_name'  ],
                            'Model'             : '',
                            'Price'             : normalize_price(product_offer['price']),
                            'Seller'            : product_offer['pro_seller'] + product_offer['seller_name'],
                            'Storage'           : product_storage if product_storage else '',
                            'Grading'           : product_offer['status'],
                            'Features'          : product_color.replace(';', ' ') if product_color else '',
                            'Timestamp'         : product_offer['timestamp'    ],
                            'Seller_grade'      : product_offer['seller_rating'],
                            'Seller_volume'     : product_offer['seller_sales' ],
                            'Delivery_price'    : product_offer['shipping'     ],
                            'Seller_description': product_offer['comments'     ],
                        })
                    writer.writerow(row_data)
        setup_ftp_file(export_data_file, export_history_file, ftp_user, ftp_group)
