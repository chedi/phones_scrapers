from re    import compile
from redis import Redis
from redis import ConnectionPool

from common.configuration import get_or_create
from common.configuration import DEFAULT_CONFIG

redis_conn        = Redis(connection_pool=ConnectionPool(host='localhost', port=6379, db=0))
post_headers      = {'Content-Type': 'application/x-www-form-urlencoded'}
base_site_url     = 'http://www.priceminister.com'
cid_pid_regex     = compile('\/mfp\/(\d+)\/.+pid=(\d+)')
phones_start_url  = 'http://www.priceminister.com/nav/Tel-PDA_Telephones-mobiles'
tablets_start_url = 'http://www.priceminister.com/nav/Informatique_tablette'
product_info_url  = 'http://www.priceminister.com/mfp/bottomadvinfo/{}/{}'
products_tree_key = 'priceminister_products_tree'
products_list_key = 'priceminister_products_list'
offer_sales_regex = compile('(\d+)')
product_url_regex = compile("advertList:'(\/mfp\?action=advlstmeta&cid=\d+&urlname=.*&htp=false&hpbs=false)'")

ftp_user             = get_or_create('priceminister_ftp_user'            , DEFAULT_CONFIG['RECOMERCE_FTP_USER'     ])
ftp_group            = get_or_create('priceminister_ftp_group'           , DEFAULT_CONFIG['RECOMERCE_FTP_GROUP'    ])
ftp_data_path        = get_or_create('priceminister_ftp_data_path'       , DEFAULT_CONFIG['RECOMERCE_FTP_DATA_PATH'])
processing_pool_size = get_or_create('priceminister_processing_pool_size', DEFAULT_CONFIG['PROCESSING_POOL_SIZE'   ])

ftp_history_path = get_or_create('priceminister_ftp_history_path', DEFAULT_CONFIG['RECOMERCE_FTP_HYSTORY_PATH'] + '/priceminister')
