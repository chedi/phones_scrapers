from __future__ import division

from time            import sleep
from pickle          import loads as ploads
from pickle          import dumps as pdumps
from os.path         import join
from datetime        import datetime
from itertools       import product
from unicodecsv      import DictWriter
from urllib3.request import urlencode

from utils.url     import get_url
from utils.url     import post_url
from utils.url     import get_cookies
from utils.export  import normalize_price
from utils.export  import setup_ftp_file
from utils.search  import SearchHelper
from export        import CSV_DEFAULT_DATA
from export        import CSV_DATA_EXPORT_HEADERS
from configuration import ftp_user
from configuration import ftp_group
from configuration import redis_conn
from configuration import prices_url
from configuration import ftp_data_path
from configuration import site_start_url
from configuration import ftp_history_path
from configuration import products_list_key
from configuration import products_tree_key
from configuration import pcs_specifications
from configuration import export_data_file_name
from configuration import export_history_file_name
from configuration import phones_tablets_specifications

from configuration import pcs_start_url
from configuration import mobiles_start_url
from configuration import tablets_start_url


def clear_redis_keys():
    for product_key in redis_conn.hgetall(products_tree_key):
        redis_conn.delete(product_key)
    redis_conn.delete(products_tree_key)
    for product_key in redis_conn.hgetall(products_list_key):
        redis_conn.delete(product_key)
    redis_conn.delete(products_tree_key)
    redis_conn.delete(products_list_key)


def explore_site_tree():
    for page_url in [mobiles_start_url, tablets_start_url]:
        page = SearchHelper(page_url, get_url(page_url, headers={'Referer': site_start_url}))

        brands = {
            _.attrs['value'] : _.text
            for _ in page.mc_find([('select', {'id':'recherche_marque_ID'}), ('option', None, 'a')])
            if _.attrs['value']}

        for brand_id, brand_name in brands.iteritems():
            brand_page_url = "{}&mar={}&mod=".format(page_url, brand_id)
            brand_page = SearchHelper(brand_page_url, get_url(brand_page_url, headers = {'Referer': page_url}))
            sleep(0.5)

            models = {
                _.attrs['value'] : _.text
                for _ in brand_page.mc_find([('select', {'id':'recherche_modele_ID'}), ('option', None, 'a')])
                if _.attrs['value']}

            for model_id, model_name in models.iteritems():
                redis_conn.hset(products_tree_key, "{}&mar={}&mod={}".format(page_url, brand_id, model_id), pdumps({
                    'brand_id'   : brand_id,
                    'model_id'   : model_id,
                    'brand_name' : brand_name,
                    'model_name' : model_name,
                    'referer_url': brand_page_url,
                }))


def scrap_pcs_worker():
    res     = []
    page    = SearchHelper(pcs_start_url, get_url(pcs_start_url, headers={'Referer': site_start_url}, response=res))
    cookies = get_cookies(res)

    brands = {
        _.attrs['value'] : _.text
        for _ in page.mc_find([('select', {'id':'recherche_marque_ID'}), ('option', None, 'a')])
        if _.attrs['value']}

    components_names   = {}
    components_options = {}

    for component_id in range(1, 6):
        component_select_id              = 'recherche_composant_{}ID'.format(component_id)
        components_names  [component_id] = page.find_attr([('select', {'id': component_select_id})], 'lib')
        components_options[component_id] = {
            _.attrs['value'] : _.text.strip()
            for _ in page.mc_find([('select', {'id': component_select_id}), ('option', None, 'a')])
            if _.attrs['value']}

    options_combinations = product(*components_options.itervalues())

    for brand_id, brand_name in brands.iteritems():
        for option_combination in options_combinations:
            for grading_name, grading_spec in pcs_specifications.iteritems():
                sleep(0.3)
                headers = {'Referer'     : pcs_start_url,
                           'Accept'      : 'text/javascript, text/html, application/xml, text/xml, */*',
                           'Content-Type': 'application/x-www-form-urlencoded',
                }

                headers.update(cookies)
                grading_spec.update({
                    'MARQUE'     : brand_id,
                    'PHPSESSID'  : cookies['Cookie'].split("=")[1],
                    'composant_1': option_combination[0],
                    'composant_2': option_combination[1],
                    'composant_3': option_combination[2],
                    'composant_4': option_combination[3],
                    'composant_5': option_combination[4],
                })

                model_name    = "-".join(option_combination)
                price_data    = post_url(prices_url, headers=headers, body=urlencode(grading_spec))
                print(brand_name, model_name, grading_name, price_data)
                product_price = price_data.split('=')[1].split("/")[0].replace('"', '').strip()

                redis_conn.hset(products_list_key, "{}-{}-{}".format(pcs_start_url, model_name, grading_name),
                    pdumps({
                        'Url'      : pcs_start_url,
                        'Name'     : "{} {}".format(brand_name, model_name),
                        'Brand'    : brand_name,
                        'Model'    : model_name,
                        'Price'    : normalize_price(product_price),
                        'Grading'  : grading_name,
                        'Timestamp': datetime.now(),
                        'Features' : {
                            components_names[1]: components_options[1][option_combination[0]],
                            components_names[2]: components_options[2][option_combination[1]],
                            components_names[3]: components_options[3][option_combination[2]],
                            components_names[4]: components_options[4][option_combination[3]],
                            components_names[5]: components_options[5][option_combination[4]],
                        }}))


def scrap_phones_tablets_worker(product_url, referer_url, product_brand, product_model):
    res = []
    get_url(product_url, headers={'Referer': referer_url}, response=res)
    cookies = get_cookies(res)

    for grading_name, grading_spec in phones_tablets_specifications.iteritems():
        sleep(0.3)

        headers = {'Referer'     : product_url,
                   'Accept'      : 'text/javascript, text/html, application/xml, text/xml, */*',
                   'Content-Type': 'application/x-www-form-urlencoded',
        }

        headers.update(cookies)
        grading_spec.update({'PHPSESSID': cookies['Cookie'].split("=")[1]})
        price_data    = post_url(prices_url, headers=headers, body=urlencode(grading_spec))
        product_price = price_data.split('=')[1].split("/")[0].replace('"', '').strip()

        redis_conn.hset(products_list_key, "{}-{}".format(product_url, grading_name),
            pdumps({
                'Url'      : product_url,
                'Name'     : "{} {}".format(product_brand, product_model),
                'Brand'    : product_brand,
                'Model'    : product_model,
                'Price'    : normalize_price(product_price),
                'Grading'  : grading_name,
                'Timestamp': datetime.now()
            }))


def scrap_products():
    for product_url, product_raw_data in redis_conn.hgetall(products_tree_key).iteritems():
        product_data = ploads(product_raw_data)
        scrap_phones_tablets_worker(product_url, product_data['referer_url'],
                                    product_data['brand_name'], product_data['model_name'])
    #scrap_pcs_worker()



def export_data():
    data = redis_conn.hgetall(products_list_key)
    if data:
        export_data_file    = join(ftp_data_path   , export_data_file_name)
        export_history_file = join(ftp_history_path, export_history_file_name.format(datetime.now()))

        with open(export_history_file, 'w') as csv_file:
            writer = DictWriter(csv_file, fieldnames=CSV_DATA_EXPORT_HEADERS, delimiter=';')
            writer.writeheader()
            for url, raw_product in data.iteritems():
                row_data     = CSV_DEFAULT_DATA.copy()
                product_data = ploads(raw_product)

                row_data.update(product_data)
                writer.writerow(row_data)
        setup_ftp_file(export_data_file, export_history_file, ftp_user, ftp_group)
