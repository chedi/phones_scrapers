# -*- coding: utf-8 -*-
from redis import Redis
from redis import ConnectionPool

from common.configuration import get_or_create
from common.configuration import DEFAULT_CONFIG

redis_conn               = Redis(connection_pool=ConnectionPool(host='localhost', port=6379, db=0))
prices_url               = 'http://www.recyclez-moi.fr/dartyonline/xml/calcul_prix.php'
pcs_start_url            = 'http://www.recyclez-moi.fr/dartyonline/home.php?pg=eval_pc'
base_site_url            = 'http://www.darty.com'
site_start_url           = 'http://www.darty.com/achat/reprise/index.html'
mobiles_start_url        = 'http://www.recyclez-moi.fr/dartyonline/home.php?pg=eval_mobile'
tablets_start_url        = 'http://www.recyclez-moi.fr/dartyonline/home.php?pg=eval_tablette'
products_tree_key        = 'darty_products_tree'
products_list_key        = 'darty_products_list'
export_data_file_name    = 'darty.csv'
export_history_file_name = 'darty_{}.csv'

phones_tablets_specifications = {
    'Fonctionnel'    :{'RETRIB': 'V', '_33': 'O', '_32': 'O', '_31': 'O'},
    'Non fonctionnel':{'RETRIB': 'V', '_33': 'N', '_32': 'O', '_31': 'O'},
    'Casse'          :{'RETRIB': 'V', '_33': 'N', '_32': 'N', '_31': 'N'},
}

pcs_specifications = {
    'Fonctionnel (avec licence)'    :{'RETRIB': 'V', '_40': 'O', '_41': 'O', '_42': 'O', '_43': 'O'},
    'Non fonctionnel (avec licence)':{'RETRIB': 'V', '_40': 'N', '_41': 'O', '_42': 'O', '_43': 'O'},
    'Casse (avec licence)'          :{'RETRIB': 'V', '_40': 'N', '_41': 'N', '_42': 'N', '_43': 'O'},
    'Fonctionnel (sans licence)'    :{'RETRIB': 'V', '_40': 'O', '_41': 'O', '_42': 'O', '_43': 'N'},
    'Non fonctionnel (sans licence)':{'RETRIB': 'V', '_40': 'N', '_41': 'O', '_42': 'O', '_43': 'N'},
    'Casse (sabs licence)'          :{'RETRIB': 'V', '_40': 'N', '_41': 'N', '_42': 'N', '_43': 'N'},
}

get_headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36',
}

ftp_user             = get_or_create('darty_ftp_user'            , DEFAULT_CONFIG['RECOMERCE_FTP_USER'     ])
ftp_group            = get_or_create('darty_ftp_group'           , DEFAULT_CONFIG['RECOMERCE_FTP_GROUP'    ])
ftp_data_path        = get_or_create('darty_ftp_data_path'       , DEFAULT_CONFIG['RECOMERCE_FTP_DATA_PATH'])
processing_pool_size = get_or_create('darty_processing_pool_size', DEFAULT_CONFIG['PROCESSING_POOL_SIZE'   ])

ftp_history_path = get_or_create('darty_ftp_history_path', DEFAULT_CONFIG['RECOMERCE_FTP_HYSTORY_PATH'] + '/darty')
