# -*- coding: utf-8 -*-
from redis import Redis
from redis import ConnectionPool

from common.configuration import get_or_create
from common.configuration import DEFAULT_CONFIG

redis_conn               = Redis(connection_pool=ConnectionPool(host='localhost', port=6379, db=0))
category_id              = '25'
section_name             = u'T\xe9l\xe9visions'
base_site_url            = 'http://www.tradeinchecker.com'
site_start_url           = 'http://www.tradeinchecker.com/portals/fnacw/index.php'
products_tree_key        = 'fnac_reprise_televisions_products_tree'
products_list_key        = 'fnac_reprise_televisions_products_list'
products_json_url        = 'http://www.tradeinchecker.com/portals/fnacw/assets/js/products.json'
export_data_file_name    = 'fnac_reprise_televisions.csv'
export_history_file_name = 'fnac_reprise_televisions_{}.csv'


get_headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36',
}


ftp_user             = get_or_create('fnac_reprise_televisions_ftp_user'            , DEFAULT_CONFIG['RECOMERCE_FTP_USER'     ])
ftp_group            = get_or_create('fnac_reprise_televisions_ftp_group'           , DEFAULT_CONFIG['RECOMERCE_FTP_GROUP'    ])
ftp_data_path        = get_or_create('fnac_reprise_televisions_ftp_data_path'       , DEFAULT_CONFIG['RECOMERCE_FTP_DATA_PATH'])
processing_pool_size = get_or_create('fnac_reprise_televisions_processing_pool_size', DEFAULT_CONFIG['PROCESSING_POOL_SIZE'   ])

ftp_history_path = get_or_create('fnac_reprise_televisions_ftp_history_path', DEFAULT_CONFIG['RECOMERCE_FTP_HYSTORY_PATH'] + '/fnac_reprise_televisions')
