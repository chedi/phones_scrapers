from redis import Redis
from redis import ConnectionPool

from common.configuration import get_or_create
from common.configuration import DEFAULT_CONFIG

redis_conn           = Redis(connection_pool=ConnectionPool(host='localhost', port=6379, db=0))
base_site_url        = 'http://www.cdiscount.com'
products_tree_key    = 'cdiscount_revente_products_tree'
products_list_key    = 'cdiscount_revente_products_list'

ftp_user             = get_or_create('cdiscount_revente_ftp_user'            , DEFAULT_CONFIG['RECOMERCE_FTP_USER'     ])
ftp_group            = get_or_create('cdiscount_revente_ftp_group'           , DEFAULT_CONFIG['RECOMERCE_FTP_GROUP'    ])
ftp_data_path        = get_or_create('cdiscount_revente_ftp_data_path'       , DEFAULT_CONFIG['RECOMERCE_FTP_DATA_PATH'])
processing_pool_size = get_or_create('cdiscount_revente_processing_pool_size', DEFAULT_CONFIG['PROCESSING_POOL_SIZE'   ])

ftp_history_path = get_or_create('cdiscount_revente_ftp_history_path', DEFAULT_CONFIG['RECOMERCE_FTP_HYSTORY_PATH'] + '/cdiscount_revente')

products_start_urls = [
    'http://www.cdiscount.com/mpv-11192-elplace.html',
    'http://www.cdiscount.com/telephonie/iphones-reconditionnes/l-14476.html',
    'http://www.cdiscount.com/telephonie/telephone-mobile/smartphones/l-1440402.html',
    'http://www.cdiscount.com/telephonie/telephone-mobile/tous-les-modeles/l-1440401.html',
    'http://www.cdiscount.com/informatique/tablettes-tactiles-ebooks/windows-8/l-1079871.html',
    'http://www.cdiscount.com/informatique/tablettes-tactiles-ebooks/apple-ipad/l-1079811.html',
    'http://www.cdiscount.com/destockage/destockage-telephonie/telephone-mobile-gsm/l-1250201.html',
    'http://www.cdiscount.com/informatique/tablettes-tactiles-ebooks/tablettes-android/l-1079880.html',
]
