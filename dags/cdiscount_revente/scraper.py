# -*- coding: utf-8 -*-
from re              import search
from pickle          import dumps as pdumps
from pickle          import loads as ploads
from os.path         import join
from datetime        import datetime
from itertools       import chain
from unicodecsv      import DictWriter

from utils.export                    import setup_ftp_file
from utils.export                    import normalize_price
from utils.search                    import SearchHelper
from utils.processing                import multiprocessing_pool
from cdiscount_revente.export        import CSV_DEFAULT_DATA
from cdiscount_revente.export        import CSV_DATA_EXPORT_HEADERS
from cdiscount_revente.configuration import ftp_user
from cdiscount_revente.configuration import ftp_group
from cdiscount_revente.configuration import redis_conn
from cdiscount_revente.configuration import ftp_data_path
from cdiscount_revente.configuration import base_site_url
from cdiscount_revente.configuration import ftp_history_path
from cdiscount_revente.configuration import products_list_key
from cdiscount_revente.configuration import products_tree_key
from cdiscount_revente.configuration import products_start_urls
from cdiscount_revente.configuration import processing_pool_size

def clear_redis_keys():
    for product_key in redis_conn.hgetall(products_list_key):
        redis_conn.delete(product_key)
    for product_key in redis_conn.hgetall(products_tree_key):
        redis_conn.delete(product_key)
    redis_conn.delete(products_tree_key)
    redis_conn.delete(products_list_key)

def explore_site_tree_worker(product_page):
    urls           = []
    page           = SearchHelper(product_page)
    product_blocks = page.mc_find([('ul', {'id': 'lpBloc'}), ('div', 'prdtBloc', 'a')])

    for product_block in product_blocks:
        url          = None
        offers_link  = product_block.find('a', class_='jsLnf')
        product_link = product_block.find('a', class_='jsQs') or product_block.find('a', class_='jsACond')


        if offers_link:
            url = offers_link.attrs['href'].split('?')[0]
            if url.startswith('#'):
                url = url.replace('#', base_site_url)
        elif product_link:
            url = product_link.attrs['href']

        if url:
            urls.append(url)
    redis_conn.hset(products_tree_key, product_page, pdumps(set(urls)))

def explore_site_tree():
    with multiprocessing_pool(processes=processing_pool_size) as pool:
        for product_start_url in products_start_urls:
            page        = SearchHelper(product_start_url)
            pages_tags  = page.find_text([('div', {'id': 'pager'}), ('li', None, 'a')])
            pages_count = int(pages_tags[-1]) if pages_tags else 0
            for page_number in range(1, pages_count + 1):
                pool.apply_async(explore_site_tree_worker,
                                 ('{}-{}.html'.format(product_start_url [:-5], page_number),))

def extract_offer_info(offer, default_price):
    pro_seller        = '(NO PRO) '
    seller_name       = ''
    rating_info       = {}
    shipping_info     = {}
    seller_name_tag   = offer.find(class_='slrName') or offer.find(class_='fpSlrName')
    shipping_info_tag = offer.find(class_='fpSlrCom')

    if (seller_name_tag             and
        seller_name_tag.find('img') and
        seller_name_tag.find('img').attrs['alt'] == u'CDiscount'):
        seller_name = 'CDiscount'

    if seller_name != 'CDiscount':
        seller_name = seller_name_tag.text.strip()
        rating_info = dict(zip(
            ['rating', 'sales'],
            [_.text for _ in offer.find(class_='slrRate').findAll('span')]))

    if shipping_info_tag:
        shipping_info = dict(zip(
            ['status', 'cost', 'availablity', 'date', 'country', 'shipper'],
            [_.text.strip() for _ in offer.find(class_='fpSlrCom').findAll('td')]))

    comments       = offer.find(class_='fpSlrAsk')
    price_tag      = offer.find('p', class_='price')
    shipping_cost  = shipping_info.get('cost', None)
    pro_seller_tag = offer.find(class_='lslrType')

    if pro_seller_tag and pro_seller_tag.text.strip() == 'VENDEUR PRO':
        pro_seller = '(PRO) '

    return {
        'price'        : normalize_price(price_tag.text if price_tag else default_price),
        'status'       : shipping_info.get('status', ''),
        'shipping'     : normalize_price(shipping_cost if shipping_cost != 'Gratuit' else 0) if shipping_cost else '',
        'comments'     : comments.text.strip() if comments else '',
        'timestamp'    : datetime.now(),
        'pro_seller'   : pro_seller,
        'seller_name'  : seller_name,
        'seller_sales' : rating_info.get('sales' , ''),
        'seller_rating': rating_info.get('rating', '')}

def scrap_products_worker(product_url):
    offers_page          = SearchHelper(product_url)
    product_details_link = offers_page.find_attr([('a', {'class': 'goToFp'})], 'href')
    product_details_page = SearchHelper(product_details_link) if product_details_link else offers_page

    product_specifications_tags = product_details_page.mc_find([('table', 'fpDescTb'), ('tbody',), ('tr', None, 'a')])
    product_specifications = {
        key.text.strip(): value.text.strip() for key, value in filter(bool, [
            spec.findAll('td') for spec in product_specifications_tags])} if product_specifications_tags else {}

    product_name          = product_details_page.find_text([('h1', {'itemprop': 'name' })])
    default_price         = product_details_page.find_text([('p' , {'itemprop': 'price'})])
    product_offers        = offers_page.mc_find([('div', 'fpTab', 'a')])
    product_storage_match = search('.+\s+(\d+)\s?G[oObB]{1}.+', product_name)

    redis_conn.hset(products_list_key, product_url, pdumps({
        'url'            : product_url,
        'rating'         : product_details_page.find_text([('span', {'itemprop': 'ratingValue'})]),
        'storage'        : product_specifications.get(u'Mémoire flash|Capacité de la mémoire interne', ''),
        'brand_name'     : product_specifications.get('Marque', ''),
        'product_name'   : product_name,
        'products_offers': filter(bool, [extract_offer_info(offer, default_price)
            for offer in product_offers]) if product_offers else []
        }))

def scrap_products():
    with multiprocessing_pool(processes=processing_pool_size) as pool:
        for product_url in chain.from_iterable(map(ploads, redis_conn.hgetall(products_tree_key).itervalues())):
            pool.apply_async(scrap_products_worker, (product_url,))

def export_data():
    data = redis_conn.hgetall(products_list_key)
    if data:
        export_data_file    = join(ftp_data_path, 'cdiscount_revente.csv')
        export_history_file = join(ftp_history_path, 'cdiscount_revente_{}.csv'.format(datetime.now()))

        with open(export_history_file, 'w') as csv_file:
            writer = DictWriter(csv_file, fieldnames=CSV_DATA_EXPORT_HEADERS, delimiter=';')
            writer.writeheader()
            for url, raw_product in data.iteritems():
                row_data     = CSV_DEFAULT_DATA.copy()
                product_data = ploads(raw_product)
                for product_offer in product_data['products_offers']:
                    if product_offer:
                        row_data.update({
                            'Url'               : product_data['url'         ],
                            'Name'              : product_data['product_name'],
                            'Brand'             : product_data['brand_name'  ],
                            'Model'             : '',
                            'Price'             : normalize_price(product_offer['price']),
                            'Seller'            : product_offer['pro_seller'] + product_offer['seller_name'],
                            'Storage'           : product_data ['storage'],
                            'Grading'           : product_offer['status' ],
                            'Features'          : '',
                            'Timestamp'         : product_offer['timestamp'    ],
                            'Seller_grade'      : product_offer['seller_rating'],
                            'Seller_volume'     : product_offer['seller_sales' ],
                            'Delivery_price'    : product_offer['shipping'     ],
                            'Seller_description': product_offer['comments'     ],
                        })
                    writer.writerow(row_data)
        setup_ftp_file(export_data_file, export_history_file, ftp_user, ftp_group)
