# -*- coding: utf-8 -*-
from brightstar_fr.structures import SectionNode
from brightstar_fr.structures import NavigationStep

IPHONES_TREE = SectionNode(
    'SelectPhone_container', 'Phone', SectionNode(
        'SelectModel_container', 'Model', SectionNode(
            'SelectColor_container', 'Color')))

IPAD_TREE = SectionNode(
    'SelectPhone_container', 'Phone', SectionNode(
        'SelectTechnology_container', 'Technology', SectionNode(
            'SelectModel_container', 'Model', SectionNode(
                'SelectColor_container', 'Color'))))

NONIOS_TREE = SectionNode(
    'SelectBrand_container', 'Brand', SectionNode(
        'SelectPhone_container', 'Phone', SectionNode(
            'SelectModel_container', 'Model', SectionNode(
                'SelectColor_container', 'Color'))))

INITIAL_STEPS = [
    (NONIOS_TREE , NavigationStep('https://appleonlinefra.mpxltd.co.uk/Nonios.aspx'  , 'nonios' , None, None, None, None)),
    (IPHONES_TREE, NavigationStep('https://appleonlinefra.mpxltd.co.uk/Default.aspx' , 'iphones', None, None, None, None)),
    (IPAD_TREE   , NavigationStep('https://appleonlinefra.mpxltd.co.uk/iPadHome.aspx', 'ipads'  , None, None, None, None)),
]

GRADING_MAP = {
    u'Fonctionnel': {
        'ctl00$ContentPlaceHolder1$repCustomerTypeSelection$ctl00$RdBtnActivityGroup4': 'RdBtnActivityYes',
        'ctl00$ContentPlaceHolder1$repCustomerTypeSelection$ctl01$RdBtnActivityGroup5': 'RdBtnActivityYes',
        'ctl00$ContentPlaceHolder1$repCustomerTypeSelection$ctl02$RdBtnActivityGroup8': 'RdBtnActivityYes',
        'ctl00$ContentPlaceHolder1$repCustomerTypeSelection$ctl03$RdBtnActivityGroup9': 'RdBtnActivityYes',
        'ctl00$ContentPlaceHolder1$repCustomerTypeSelection$ctl05$RdBtnActivityGroup6': 'RdBtnActivityYes',
    },
    u'Casse': {
        'ctl00$ContentPlaceHolder1$repCustomerTypeSelection$ctl00$RdBtnActivityGroup4': 'RdBtnActivityYes',
        'ctl00$ContentPlaceHolder1$repCustomerTypeSelection$ctl01$RdBtnActivityGroup5': 'RdBtnActivityNo' ,
        'ctl00$ContentPlaceHolder1$repCustomerTypeSelection$ctl02$RdBtnActivityGroup8': 'RdBtnActivityYes',
        'ctl00$ContentPlaceHolder1$repCustomerTypeSelection$ctl03$RdBtnActivityGroup9': 'RdBtnActivityYes',
        'ctl00$ContentPlaceHolder1$repCustomerTypeSelection$ctl05$RdBtnActivityGroup6': 'RdBtnActivityYes',
}}
