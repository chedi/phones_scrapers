from pickle          import dumps as pdumps
from pickle          import loads as ploads
from os.path         import join
from datetime        import datetime
from unicodecsv      import DictWriter
from urllib3.request import urlencode

from utils.url                   import post_url
from utils.url                   import get_cookies
from utils.url                   import get_all_hidden_form_vars
from utils.common                import merge_dicts
from utils.export                import normalize_price
from utils.export                import setup_ftp_file
from utils.search                import SearchHelper
from utils.processing            import multiprocessing_pool
from brightstar_fr.export        import CSV_DEFAULT_DATA
from brightstar_fr.export        import CSV_DATA_EXPORT_HEADERS
from brightstar_fr.structures    import NavigationStep
from brightstar_fr.specification import GRADING_MAP
from brightstar_fr.specification import INITIAL_STEPS
from brightstar_fr.configuration import ftp_user
from brightstar_fr.configuration import ftp_group
from brightstar_fr.configuration import redis_conn
from brightstar_fr.configuration import post_headers
from brightstar_fr.configuration import ftp_data_path
from brightstar_fr.configuration import site_step_2_url
from brightstar_fr.configuration import ftp_history_path
from brightstar_fr.configuration import products_list_key
from brightstar_fr.configuration import products_tree_key
from brightstar_fr.configuration import processing_pool_size

def clear_redis_keys():
    for product_key in redis_conn.hgetall(products_list_key):
        redis_conn.delete(product_key)
    redis_conn.delete(products_tree_key)
    redis_conn.delete(products_list_key)

def get_form_params(page, manager):
    return merge_dicts(get_all_hidden_form_vars(page), {
        '__ASYNCPOST'         : 'false',
        '__EVENTTARGET'       : manager,
        'ctl00$ScriptManager1': 'ctl00$ContentPlaceHolder1$UPMain|{}'.format(manager)})

def get_selection_options(page, selection_container):
    return map(
        lambda _: (_['id'].replace('_', '$'), _['name'], page.find_text([('label', {'for': _['id']})])),
        filter(lambda _: _['disabled'] is None,
               page.find_attr([('div', {'id': selection_container}), ('input', {'type':'radio'}, 'a')],
                              ['id', 'name', 'disabled'])))

def build_navigation_tree(previous_step, section_node, headers={}, section_page=None):
    if section_page is None:
        req          = []
        section_page = SearchHelper(previous_step.url, post_url(previous_step.url, post_headers, {}, req))
        headers      = merge_dicts(headers, get_cookies(req))

    headers       = merge_dicts(post_headers, headers)
    sections_data = get_selection_options(section_page, section_node.current_node_name)

    if section_node.next_node:
        for section_option, section_name, section_value in sections_data:
            step = NavigationStep(previous_step.url, section_name, section_option,
                                  section_value, previous_step, section_node)

            next_section_page = SearchHelper(step.url,
                post_url(step.url, headers, urlencode(merge_dicts(
                    step.get_previous_options(), get_form_params(section_page, section_option),
                    {section_name: section_page.find_attr([('input', {'name': section_name})], 'value')}))))

            build_navigation_tree(step, section_node.next_node, headers, next_section_page)
    else:
        map(lambda _: NavigationStep(previous_step.url, _[1], _[0], _[2], previous_step, section_node),
            sections_data)

def explore_site_tree():
    paths = []
    for navigation_tree, initial_step in INITIAL_STEPS:
        build_navigation_tree(initial_step, navigation_tree)
        paths += initial_step.get_all_paths()
    redis_conn.set(products_tree_key, pdumps(paths))

def scrap_products_worker(step_1_navigation_paths, grade_name, grading_specification):
    req     = []
    url     = step_1_navigation_paths[0].url
    page    = SearchHelper(url, post_url(url, post_headers, {}, req))
    params  = {}
    headers = merge_dicts(post_headers, get_cookies(req))

    for path in step_1_navigation_paths:
        params = merge_dicts(
            params, get_form_params(page, path.option),
            {path.name: page.find_attr([('input', {'name': path.name})], 'value')})
        page = SearchHelper(path.url, post_url(path.url, headers, urlencode(params), req))

    params = merge_dicts(params, get_form_params(page, page.find_attr([
        ('div', {'id': 'SelectHandset_btnGroup'}), ('a',)], 'id').replace('_', '$')),
        {path.name: page.find_attr([('input', {'name': path.name})], 'value')})
    page = SearchHelper(path.url, post_url(path.url, headers, urlencode(params), req))

    params = merge_dicts(grading_specification, get_form_params(page, page.find_attr([
        ('div', {'id': 'InspectionContainer_btnGroup'}), ('a',)], 'id').replace('_', '$')),
        {path.name: page.find_attr([('input', {'name': path.name})], 'value')})
    page = SearchHelper(site_step_2_url, post_url(site_step_2_url, headers, urlencode(params), req))

    data       = {path.section_node.node_type: path.value for path in step_1_navigation_paths}
    price      = page.find_text([('div', {'id':'SelectedValuation_container'}), ('p',)])
    brand      = data.get('Brand'     , 'Apple')
    model      = data.get('Model'     , '')
    color      = data.get('Color'     , 'undefined')
    product    = data.get('Phone'     , 'undefined')
    technology = data.get('Technology', 'undefined')

    redis_conn.hset(products_list_key, u'{}-{}-{}-{}-{}-{}'.format(brand, product, model, technology, color, grade_name), pdumps({
        'Url'      : url,
        'Name'     : product,
        'Brand'    : brand,
        'Model'    : '',
        'Price'    : normalize_price(price.strip().split()[0]),
        'Grading'  : grade_name,
        'Storage'  : model,
        'Features' : u"Color: {} - Network: {}".format(color, technology),
        'Timestamp': datetime.now(),
    }))

def scrap_products():
    with multiprocessing_pool(processes=processing_pool_size) as pool:
        for step_1_navigation_path in ploads(redis_conn.get(products_tree_key)):
            for grade_name, grade_specification in GRADING_MAP.iteritems():
                pool.apply_async(scrap_products_worker,
                    (step_1_navigation_path, grade_name, grade_specification))

def export_data():
    data = redis_conn.hgetall(products_list_key)
    if data:
        export_data_file    = join(ftp_data_path, 'brightstar_fr.csv')
        export_history_file = join(ftp_history_path, 'brightstar_fr_{}.csv'.format(datetime.now()))

        with open(export_history_file, 'w') as csv_file:
            writer = DictWriter(csv_file, fieldnames=CSV_DATA_EXPORT_HEADERS, delimiter=';')
            writer.writeheader()
            for url, raw_product in data.iteritems():
                row_data     = CSV_DEFAULT_DATA.copy()
                product_data = ploads(raw_product)
                row_data.update(product_data)
                writer.writerow(row_data)
        setup_ftp_file(export_data_file, export_history_file, ftp_user, ftp_group)
