from __future__ import division

from re         import findall
from re         import search
from math       import ceil
from json       import loads as jloads
from json       import dumps as jdumps
from pickle     import dumps as pdumps
from pickle     import loads as ploads
from base64     import b64decode
from os.path    import join
from datetime   import datetime
from unicodecsv import DictWriter

from utils.url                  import post_url
from utils.export               import normalize_price
from utils.export               import setup_ftp_file
from utils.search               import SearchHelper
from utils.processing           import multiprocessing_pool
from fnac_revente.export        import CSV_DEFAULT_DATA
from fnac_revente.export        import CSV_DATA_EXPORT_HEADERS
from fnac_revente.configuration import ftp_user
from fnac_revente.configuration import ftp_group
from fnac_revente.configuration import redis_conn
from fnac_revente.configuration import ftp_data_path
from fnac_revente.configuration import ftp_history_path
from fnac_revente.configuration import phones_start_url
from fnac_revente.configuration import tablets_start_url
from fnac_revente.configuration import products_list_key
from fnac_revente.configuration import products_tree_key
from fnac_revente.configuration import processing_pool_size

def clear_redis_keys():
    for product_key in redis_conn.hgetall(products_tree_key):
        redis_conn.delete(product_key)
    for product_key in redis_conn.hgetall(products_list_key):
        redis_conn.delete(product_key)
    redis_conn.delete(products_tree_key)
    redis_conn.delete(products_list_key)

def get_products_pages_info(page):
    start_item, items_by_page, items_count = map(
        int, findall('(\d+)', page.find_text([('span', 'resultPosition')])))
    pages_count = int(ceil(items_count / items_by_page))
    return items_count, items_by_page, pages_count

def explore_site_tree_worker(base_url, product_page):
    url  = '{}?PageIndex={}'.format(base_url, product_page)
    page = SearchHelper(url)
    redis_conn.hset(products_tree_key, url, pdumps([
        product.find('a').attrs['href'] for product in page.mc_find([('p', 'Article-desc', 'a')])]
    ))

def explore_site_tree():
    phones_page  = SearchHelper(phones_start_url )
    tablets_page = SearchHelper(tablets_start_url)
    phones_count , phones_per_page , phones_pages_count  = get_products_pages_info(phones_page )
    tablets_count, tablets_per_page, tablets_pages_count = get_products_pages_info(tablets_page)

    with multiprocessing_pool(processes=processing_pool_size) as pool:
        for phones_page in range(1, phones_pages_count + 1):
            pool.apply_async(explore_site_tree_worker, (phones_start_url, phones_page))
        for tablets_page in range(1, tablets_pages_count + 1):
            pool.apply_async(explore_site_tree_worker, (tablets_start_url, tablets_page))

def extract_offer_info(offer):
    tds = offer.findAll('td')

    if len(tds) == 4:
        pro_seller    = '(NO PRO) '
        shipping      = ''
        seller_logo   = tds[2].find(class_='logo').find('img')
        shipping_tag  = tds[0].find('strong', class_='noir') or tds[0].find('p', class_='mrg_no')
        seller_sales  = ''
        seller_rating = ''

        if shipping_tag:
            shipping = shipping_tag.text if shipping_tag.text.strip().replace(u'\xa0', ' ') != '+ Livraison gratuite' else 0

        if seller_logo and seller_logo.attrs['alt'] == 'Fnac.com':
            pro_seller  = '(PRO) '
            seller_name = 'Fnac'
        else:
            seller_name         = tds[2].findAll('span')[0].find('strong').text.strip()
            seller_sales_text   = tds[2].findAll('span')[0].text.split('-')[-1].strip()
            seller_sales_match  = findall(r'(\d+)', seller_sales_text)
            seller_rating_image = tds[2].findAll('span')[0].find('img')

            if seller_rating_image:
                seller_rating = seller_rating_image.attrs['title'].split(':')[-1].strip()

            if seller_sales_match:
                seller_sales = int(seller_sales_match[0])

            if seller_logo and seller_logo.attrs['alt'] == 'Vendeur Pro':
                pro_seller = '(PRO) '

        return {
            'price'        : tds[0].find('strong', class_='userPrice').text.strip(),
            'status'       : tds[1].text.strip(),
            'shipping'     : normalize_price(shipping),
            'comments'     : ' '.join(tds[2].findAll('span')[1].text.split()).strip(),
            'timestamp'    : datetime.now(),
            'pro_seller'   : pro_seller,
            'seller_name'  : seller_name,
            'seller_sales' : seller_sales,
            'seller_rating': seller_rating}

def scrap_products_worker(product_url):
    page       = SearchHelper(product_url)
    body_attrs = page.mc_find([('body',)]).attrs
    offers_tag = page.mc_find([('div', {'id': 'usedOffers'})])

    if offers_tag:
        offers_attrs = offers_tag.attrs
    else:
        return

    request_body = {
        "data": {
            "content"    :{"listoccazoffers":{"itemsperpage":1000,"order":7,"pagenum":1,"status":2}},
            "environment":{
                "id"     : body_attrs['data-itemid'],
                "url"    : product_url,
                "prid"   : offers_attrs['data-product-id'],
                "catalog": offers_attrs['data-catalog-id'],
    }}}

    raw_response_data = post_url(
        url     = "http://www.fnac.com/api/product/v1/GetOccasOffers",
        body    = jdumps(request_body),
        headers = {'Accept': 'application/json, text/javascript, */*; q=0.01'}
    )

    data       = jloads(raw_response_data)
    data_page  = SearchHelper(product_url, data['data']['content']['listoccazoffers']['html'].replace('\r\n', ''))
    offers_url = data_page.find_attr([('a', {'class': 'ProductSellers-OffersButton'})], 'href')

    if offers_url:
        details_page          = SearchHelper(offers_url)
        product_name          = page.find_text([('span', {'itemprop': 'name'})])
        product_offers        = details_page.mc_find([('table', {'id': 'colsMP'}), ('tbody',), ('tr', None, 'a')])
        product_storage_match = search('.+\s+(\d+) G[oO]{1}(.+|$)', product_name)

        redis_conn.hset(products_list_key, product_url, pdumps({
            'url'            : product_url,
            'rating'         : page.find_text([('meta', {'itemprop': 'ratingValue'})]),
            'storage'        : product_storage_match.group(1) if product_storage_match else '',
            'brand_name'     : page.find_text([('span', {'itemprop': 'manufacturer'})]),
            'product_name'   : product_name,
            'products_offers': filter(bool, [extract_offer_info(offer)
                for offer in product_offers]) if product_offers else []
        }))

def scrap_products():
    with multiprocessing_pool(processes=processing_pool_size) as pool:
        for page_url, raw_products_urls in redis_conn.hgetall(products_tree_key).iteritems():
            for product_url in ploads(raw_products_urls):
                pool.apply_async(scrap_products_worker, (product_url,))

def export_data():
    data = redis_conn.hgetall(products_list_key)
    if data:
        export_data_file    = join(ftp_data_path, 'fnac_revente.csv')
        export_history_file = join(ftp_history_path, 'fnac_revente_{}.csv'.format(datetime.now()))

        with open(export_history_file, 'w') as csv_file:
            writer = DictWriter(csv_file, fieldnames=CSV_DATA_EXPORT_HEADERS, delimiter=';')
            writer.writeheader()
            for url, raw_product in data.iteritems():
                row_data     = CSV_DEFAULT_DATA.copy()
                product_data = ploads(raw_product)

                for product_offer in product_data['products_offers']:
                    if product_offer:
                        row_data.update({
                            'Url'               : product_data['url'         ],
                            'Name'              : product_data['product_name'],
                            'Brand'             : product_data['brand_name'  ],
                            'Model'             : '',
                            'Price'             : normalize_price(product_offer['price']),
                            'Seller'            : product_offer['pro_seller' ] + product_offer['seller_name'],
                            'Grading'           : product_offer['status'     ],
                            'Storage'           : '',
                            'Features'          : '',
                            'Timestamp'         : product_offer['timestamp'    ],
                            'Seller_grade'      : product_offer['seller_rating'],
                            'Seller_volume'     : product_offer['seller_sales' ],
                            'Delivery_price'    : product_offer['shipping'     ],
                            'Seller_description': product_offer['comments'     ],
                        })
                    writer.writerow(row_data)
        setup_ftp_file(export_data_file, export_history_file, ftp_user, ftp_group)
