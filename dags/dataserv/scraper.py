from csv               import DictWriter
from pickle            import dumps as pdumps
from pickle            import loads as ploads
from os.path           import join
from datetime          import datetime
from urllib3.request   import urlencode

from utils.url              import post_url
from utils.url              import get_all_hidden_form_vars
from utils.url              import get_cookies
from utils.search           import SearchHelper
from utils.export           import setup_ftp_file
from utils.export           import normalize_price
from utils.strings          import normalize_string
from dataserv.export        import CSV_DEFAULT_DATA
from dataserv.export        import CSV_DATA_EXPORT_HEADERS
from utils.processing       import multiprocessing_pool
from dataserv.structures    import NavigationStep
from dataserv.structures    import GradingSpecification
from dataserv.specification import path_filter
from dataserv.specification import GRADING_MAP
from dataserv.specification import SECTIONS_TREE
from dataserv.configuration import ftp_user
from dataserv.configuration import ftp_group
from dataserv.configuration import redis_conn
from dataserv.configuration import initial_step
from dataserv.configuration import post_headers
from dataserv.configuration import ftp_data_path
from dataserv.configuration import base_site_url
from dataserv.configuration import site_step_2_url
from dataserv.configuration import ftp_history_path
from dataserv.configuration import products_list_key
from dataserv.configuration import products_tree_key
from dataserv.configuration import processing_pool_size

def clear_redis_keys():
    for product_key in redis_conn.hgetall(products_list_key):
        redis_conn.delete(product_key)
    redis_conn.delete(products_tree_key)
    redis_conn.delete(products_list_key)

def get_overlay_options(overlay_id, page):
    return {
        _.attrs['href'].split("'")[1] : _.text
        for _ in page.mc_find([('div', {'id': overlay_id}), ('a', None ,'a')])}

def get_form_params(page, manager):
    return dict({
        '__ASYNCPOST'              : 'false',
        '__EVENTTARGET'            : manager,
        'ctl00$Main$ScriptManager1': 'ctl00$Main$UpdatePanelMain|{}'.format(manager)},
        ** get_all_hidden_form_vars(page))

def build_navigation_tree(url, section_node=SECTIONS_TREE, section_page=None, previous_step=initial_step):
    if section_page is None:
        section_page = SearchHelper(url, post_url(url, post_headers))
    sections_data = get_overlay_options(section_node.current_node_name, section_page)

    if section_node.next_node:
        for section_option, section_name in sections_data.iteritems():
            build_navigation_tree(url, section_node.next_node,
                SearchHelper(url, post_url(
                    url, post_headers, urlencode(
                        get_form_params(section_page, section_option)))),
                NavigationStep(section_name, section_option, previous_step, section_node))
    else:
        map(
            lambda _: NavigationStep(_[1], _[0], previous_step, section_node),
            sections_data.iteritems())

def explore_site_tree():
    build_navigation_tree(base_site_url)
    redis_conn.set(products_tree_key, pdumps(initial_step.get_filtred_paths(path_filter)))

def page_transistion(url, page, headers):
    form_params = get_all_hidden_form_vars(page)
    form_params.update ({
        '__EVENTTARGET'           :  '',
        '__EVENTARGUMENT'         :  '',
        'ctl00$Main$BtnContinue.x': '0',
        'ctl00$Main$BtnContinue.y': '0',
    })
    body_data = urlencode(form_params)
    return SearchHelper(url, post_url(url, headers, body_data))

def scrap_products_worker(step_1_navigation_paths, step_2_grading_specification):
    req     = []
    page    = SearchHelper(base_site_url, post_url(base_site_url, post_headers, {}, req))
    headers = dict(post_headers, **get_cookies(req))

    for path in step_1_navigation_paths:
        page = SearchHelper(base_site_url, post_url(
            base_site_url, headers, urlencode(
                get_form_params(page, path.option)), req))

    page             = page_transistion(base_site_url, page, headers)
    section_iterator = step_2_grading_specification.options_tree

    while section_iterator:
        for option, option_label in map(
            lambda _: (_.attrs['id'].replace('_', '$'), _.text),
            page.mc_find([('div', {'id': section_iterator.current_node_name}), ('a', None, 'a')])
        ):
            if normalize_string(option_label) == step_2_grading_specification.specification[section_iterator.node_type]:
                break
        else:
            raise Exception('No option matches this spcification')
        page = SearchHelper(site_step_2_url, post_url(
            site_step_2_url, headers, urlencode(
                get_form_params(page, option)), req))
        section_iterator = section_iterator.next_node

    page    = page_transistion(site_step_2_url, page, headers)
    price   = page.find_text([('span', {'id':'ctl00_Main_LabelEstimatedTIV'})])
    product = normalize_string(step_1_navigation_paths[3].name)
    grading = normalize_string(step_2_grading_specification.name)

    redis_conn.hset(products_list_key, product + grading, pdumps({
        'Url'      : base_site_url,
        'Name'     : product,
        'Brand'    : normalize_string(step_1_navigation_paths[1].name),
        'Model'    : normalize_string(step_1_navigation_paths[2].name),
        'Price'    : normalize_price(price.split()[1]),
        'Grading'  : grading,
        'Timestamp': datetime.now(),
    }))

def scrap_products():
    step_1_navigation_paths = ploads(redis_conn.get(products_tree_key))

    with multiprocessing_pool(processes=processing_pool_size) as pool:
        for step_1_navigation_path in step_1_navigation_paths:
            for grading_specifications in GRADING_MAP.values():
                if grading_specifications['identificator'](
                    step_1_navigation_path[0].name,
                    step_1_navigation_path[1].name,
                    step_1_navigation_path[2].name
                ):
                    break
            else:
                raise Exception('Unable to identify this path')

            for grade_name, grade_specification in grading_specifications['specifications'].iteritems():
                pool.apply_async(scrap_products_worker,
                                 (step_1_navigation_path,
                                  GradingSpecification(grade_name,
                                                       grade_specification,
                                                       grading_specifications['tree'])))

def export_data():
    data = redis_conn.hgetall(products_list_key)
    if data:
        export_data_file    = join(ftp_data_path, 'dataserv.csv')
        export_history_file = join(ftp_history_path, 'dataserv_{}.csv'.format(datetime.now()))

        with open(export_history_file, 'w') as csv_file:
            writer = DictWriter(csv_file, fieldnames=CSV_DATA_EXPORT_HEADERS, delimiter=';')
            writer.writeheader()
            for url, raw_product in data.iteritems():
                row_data     = CSV_DEFAULT_DATA.copy()
                product_data = ploads(raw_product)
                row_data.update(product_data)
                writer.writerow(row_data)
        setup_ftp_file(export_data_file, export_history_file, ftp_user, ftp_group)
