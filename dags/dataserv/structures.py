from collections import namedtuple


class NavigationStep(object):
    def __init__(self, name, option, previous_step, section_node=None):
        self.name          = name
        self.option        = option
        self.next_steps    = list()
        self.section_node  = section_node
        self.previous_step = previous_step
        if self.previous_step:
            self.previous_step.next_steps.append(self)

    def __str__(self):
        return self.name

    def get_all_paths(self):
        iterator  = self
        all_paths = []

        while iterator.previous_step:
            iterator = iterator.previous_step

        def get_sub_paths(step, paths):
            if step.next_steps:
                for sub_step in step.next_steps:
                    get_sub_paths(sub_step, paths)
            else:
                results = []
                while step.previous_step:
                    results.append(step)
                    step = step.previous_step
                results.reverse()
                paths.append(results)
        get_sub_paths(iterator, all_paths)
        return all_paths

    def get_filtred_paths(self, path_filter):
        results = []
        for path in self.get_all_paths():
            product      = path[2].name
            category     = path[0].name
            manufacturer = path[1].name

            if path_filter(category, manufacturer, product):
                results.append(path)
        return results


class SectionNode(object):
    def __init__(self, current_node, node_type, next_node=None):
        self.next_node    = next_node
        self.node_type    = node_type
        self.current_node = current_node

    @property
    def next_node_name(self):
        if self.next_node:
            return self.next_node.current_node

    @property
    def current_node_name(self):
        return self.current_node


GradingSpecification = namedtuple('GradingSpecification', ['name', 'specification', 'options_tree'])
