# -*- coding: utf-8 -*-
from utils.strings       import normalize_string
from utils.strings       import normalize_strings
from dataserv.structures import SectionNode


def path_filter(category, manufacturer, product):
    product      = normalize_string(product)
    category     = normalize_string(category)
    manufacturer = normalize_string(manufacturer)

    return ((
        manufacturer == 'apple' and
        category == normalize_string(u'ordenador portátil') and
        product not in[u'ibook', u'powerbook']
    ) or category in normalize_strings([u'smartphone', u'tablet']))



SECTIONS_TREE = SectionNode(
    'divCbxCategoryOverlay', 'Category', SectionNode(
        'divCbxManufacturerOverlay', 'Manufacturer', SectionNode(
            'divCbxProductGroupOverlay', 'Product', SectionNode(
                'divCbxArticleOverlay', 'Article'
))))

GRADING_MAP = {
    'laptops': {
        'identificator' : lambda category, manufacturer, product: normalize_string(category) == normalize_string(u'ordenador portátil'),
        'specifications': {
            'Comme neuf': {
                'Attribute1': normalize_string(u'Sí'),
                'Attribute2': normalize_string(u'Sí'),
                'Attribute3': normalize_string(u'Excelente'),
                'Attribute4': normalize_string(u'Excelente'),
            },
            'Fonctionnel': {
                'Attribute1': normalize_string(u'Sí'),
                'Attribute2': normalize_string(u'Sí'),
                'Attribute3': normalize_string(u'Aceptable'),
                'Attribute4': normalize_string(u'Aceptable'),
            },
            'Non Fonctionnel': {
                'Attribute1': normalize_string(u'Sí'),
                'Attribute2': normalize_string(u'Sí'),
                'Attribute3': normalize_string(u'Malo'),
                'Attribute4': normalize_string(u'Malo'),
            },
            'Cassé': {
                'Attribute1': normalize_string(u'No'),
                'Attribute2': normalize_string(u'No'),
                'Attribute3': normalize_string(u'Malo'),
                'Attribute4': normalize_string(u'Malo'),
        }},
        'tree': SectionNode(
            'ctl00_Main_RepeaterArticleAttributes_ctl00_PanelAttributeDropDownListOverlay', 'Attribute1', SectionNode(
                'ctl00_Main_RepeaterArticleAttributes_ctl01_PanelAttributeDropDownListOverlay', 'Attribute2', SectionNode(
                    'ctl00_Main_RepeaterArticleAttributes_ctl02_PanelAttributeDropDownListOverlay', 'Attribute3', SectionNode(
                        'ctl00_Main_RepeaterArticleAttributes_ctl03_PanelAttributeDropDownListOverlay', 'Attribute4'
    ))))},
    'smartphones apple': {
        'identificator' : lambda category, manufacturer, product: normalize_string(category) == normalize_string(u'smartphone') and normalize_string(manufacturer) == 'apple',
        'specifications': {
            'Fonctionnel': {
                'Attribute1': normalize_string(u'No'),
                'Attribute2': normalize_string(u'No, está perfecto'),
                'Attribute3': normalize_string(u'Sí'),
            },
            'Non Fonctionnel': {
                'Attribute1': normalize_string(u'No'),
                'Attribute2': normalize_string(u'No, está perfecto'),
                'Attribute3': normalize_string(u'No'),
            },
            'Cassé': {
                'Attribute1': normalize_string(u'No'),
                'Attribute2': normalize_string(u'Sí, el cristal está roto'),
                'Attribute3': normalize_string(u'Sí'),
        }},
        'tree': SectionNode(
            'ctl00_Main_RepeaterArticleAttributes_ctl00_PanelAttributeDropDownListOverlay', 'Attribute1', SectionNode(
                'ctl00_Main_RepeaterArticleAttributes_ctl01_PanelAttributeDropDownListOverlay', 'Attribute2', SectionNode(
                    'ctl00_Main_RepeaterArticleAttributes_ctl02_PanelAttributeDropDownListOverlay', 'Attribute3'
    )))},
     'other smartphones': {
        'identificator' : lambda category, manufacturer, product: normalize_string(category) == normalize_string(u'smartphone') and normalize_string(manufacturer) != ('apple'),
        'specifications': {
            'Fonctionnel': {
                'Attribute1': normalize_string(u'No, está perfecto'),
                'Attribute2': normalize_string(u'No'),
                'Attribute3': normalize_string(u'Sí'),
            },
            'Non Fonctionnel': {
                'Attribute1': normalize_string(u'No, está perfecto'),
                'Attribute2': normalize_string(u'No'),
                'Attribute3': normalize_string(u'No'),
            },
            'Cassé': {
                'Attribute1': normalize_string(u'Sí, el cristal está roto'),
                'Attribute2': normalize_string(u'No'),
                'Attribute3': normalize_string(u'Sí'),
        }},
        'tree': SectionNode(
            'ctl00_Main_RepeaterArticleAttributes_ctl00_PanelAttributeDropDownListOverlay', 'Attribute1', SectionNode(
                'ctl00_Main_RepeaterArticleAttributes_ctl01_PanelAttributeDropDownListOverlay', 'Attribute2', SectionNode(
                    'ctl00_Main_RepeaterArticleAttributes_ctl02_PanelAttributeDropDownListOverlay', 'Attribute3'
    )))},
    'ipads': {
        'identificator' : lambda category, manufacturer, product: normalize_string(category) == normalize_string(u'tablet') and normalize_string(manufacturer) == ('apple'),
        'specifications': {
            'Fonctionnel': {
                'Attribute1': normalize_string(u'No'),
                'Attribute2': normalize_string(u'No'),
                'Attribute3': normalize_string(u'Sí'),
            },
            'Non Fonctionnel': {
                'Attribute1': normalize_string(u'No'),
                'Attribute2': normalize_string(u'No'),
                'Attribute3': normalize_string(u'No'),
            },
            'Cassé': {
                'Attribute1': normalize_string(u'Sí'),
                'Attribute2': normalize_string(u'No'),
                'Attribute3': normalize_string(u'Sí'),
        }},
        'tree': SectionNode(
            'ctl00_Main_RepeaterArticleAttributes_ctl00_PanelAttributeDropDownListOverlay', 'Attribute1', SectionNode(
                'ctl00_Main_RepeaterArticleAttributes_ctl01_PanelAttributeDropDownListOverlay', 'Attribute2', SectionNode(
                    'ctl00_Main_RepeaterArticleAttributes_ctl02_PanelAttributeDropDownListOverlay', 'Attribute3'
    )))},
    'other tablets': {
        'identificator' : lambda category, manufacturer, product: normalize_string(category) == normalize_string(u'tablet') and normalize_string(manufacturer) != ('apple'),
        'specifications': {
            'Fonctionnel': {
                'Attribute1': normalize_string(u'No, está perfecto'),
                'Attribute2': normalize_string(u'No'),
                'Attribute3': normalize_string(u'Sí'),
            },
            'Non Fonctionnel': {
                'Attribute1': normalize_string(u'No, está perfecto'),
                'Attribute2': normalize_string(u'No'),
                'Attribute3': normalize_string(u'No'),
            },
            'Cassé': {
                'Attribute1': normalize_string(u'Sí, el cristal está roto'),
                'Attribute2': normalize_string(u'No'),
                'Attribute3': normalize_string(u'Sí'),
        }},
        'tree': SectionNode(
            'ctl00_Main_RepeaterArticleAttributes_ctl00_PanelAttributeDropDownListOverlay', 'Attribute1', SectionNode(
                'ctl00_Main_RepeaterArticleAttributes_ctl01_PanelAttributeDropDownListOverlay', 'Attribute2', SectionNode(
                    'ctl00_Main_RepeaterArticleAttributes_ctl02_PanelAttributeDropDownListOverlay', 'Attribute3'
    )))}
}
