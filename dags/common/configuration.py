from json import dumps as jdumps
from json import loads as jloads

from common.models import Variable


def get_or_create(variable_name, default_value=None, json_serialize=True):
    result = Variable.get(variable_name)
    if not result:
        if not default_value:
            default_value = DEFAULT_CONFIG.get(variable_name, None)
        if default_value:
            Variable.set(
                variable_name,
                jdumps(default_value) if json_serialize else default_value)
    elif json_serialize:
        return jloads(result)
    return result

DEFAULT_CONFIG = {
    'ADMINS_EMAILS'             : ['chedi.toueiti@gmail.com'],
    'RECOMERCE_FTP_USER'        : 'recommerce',
    'RECOMERCE_FTP_GROUP'       : 'recommerce',
    'PROCESSING_POOL_SIZE'      : 20,
    'RECOMERCE_FTP_DATA_PATH'   : '/home/recommerce/datajail/data',
    'RECOMERCE_FTP_HYSTORY_PATH': '/home/recommerce/datajail/history',
}
