from json           import loads
from json           import dumps
from sqlalchemy     import literal
from airflow.utils  import provide_session
from airflow.models import Variable as AirflowVariable


class Variable(AirflowVariable):
    @classmethod
    @provide_session
    def get(cls, key, session, deserialize_json=False):
        result = session.query(cls).filter(cls.key == key).first()
        if result:
            value = result.val
            if deserialize_json and value:
                value = loads(value)
            return value

    @classmethod
    @provide_session
    def set(cls, key, value, session, serialize_json=False):
        if serialize_json:
            value = dumps(value)
        key_exisits = session.query(literal(True)).filter(
            session.query(cls).filter(cls.key == key).exists()).scalar()

        if key_exisits:
            variable = session.query(cls).filter(cls.key == key).one()
            variable.val = value
        else:
            variable = cls(key=key, val=value)
        session.add(variable)
        session.commit()
