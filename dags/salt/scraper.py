from __future__ import division

from pickle     import loads as ploads
from pickle     import dumps as pdumps
from os.path    import join
from datetime   import datetime
from unicodecsv import DictWriter

from utils.export  import normalize_price
from utils.export  import setup_ftp_file
from utils.search  import SearchHelper
from export        import CSV_DEFAULT_DATA
from export        import CSV_DATA_EXPORT_HEADERS
from configuration import ftp_user
from configuration import ftp_group
from configuration import redis_conn
from configuration import ftp_data_path
from configuration import base_site_url
from configuration import site_start_url
from configuration import ftp_history_path
from configuration import products_list_key
from configuration import products_tree_key
from configuration import export_data_file_name
from configuration import export_history_file_name


def clear_redis_keys():
    for product_key in redis_conn.hgetall(products_tree_key):
        redis_conn.delete(product_key)
    redis_conn.delete(products_tree_key)
    for product_key in redis_conn.hgetall(products_list_key):
        redis_conn.delete(product_key)
    redis_conn.delete(products_tree_key)
    redis_conn.delete(products_list_key)


def explore_site_tree():
    page = SearchHelper(site_start_url)

    redis_conn.hmset(products_tree_key, {
        _.attrs["value"] : base_site_url + _.attrs["data-url"]
        for _ in page.mc_find([("select", {"id": "cashback-phone-search"}), ("option", None, "a")])
        if "data-url" in _.attrs})


def scrap_products_worker(product_url):
    page             = SearchHelper(product_url)
    product_name     = page.find_text([("h2", "univers font-16")])
    product_prices   = page.find_text([("form", {"id": "product-selection"}), ("span", "price", "a")])
    product_gradings = page.find_text([("form", {"id": "product-selection"}), ("h3", "superior", "a")])

    for price, grading in zip(product_prices, product_gradings):
        redis_conn.hset(products_list_key, "{}-{}".format(product_url, grading),
            pdumps({
                'Url'      : product_url,
                'Name'     : product_name,
                'Brand'    : product_name.split()[0],
                'Model'    : " ".join(product_name.split()[1:]),
                'Price'    : normalize_price(price.split(u"\u2013")[0]),
                'Grading'  : grading,
                'Timestamp': datetime.now()
            }))


def scrap_products():
    for product_url in redis_conn.hgetall(products_tree_key).itervalues():
        scrap_products_worker(product_url)


def export_data():
    data = redis_conn.hgetall(products_list_key)
    if data:
        export_data_file    = join(ftp_data_path   , export_data_file_name)
        export_history_file = join(ftp_history_path, export_history_file_name.format(datetime.now()))

        with open(export_history_file, 'w') as csv_file:
            writer = DictWriter(csv_file, fieldnames=CSV_DATA_EXPORT_HEADERS, delimiter=';')
            writer.writeheader()
            for url, raw_product in data.iteritems():
                row_data     = CSV_DEFAULT_DATA.copy()
                product_data = ploads(raw_product)

                row_data.update(product_data)
                writer.writerow(row_data)
        setup_ftp_file(export_data_file, export_history_file, ftp_user, ftp_group)
