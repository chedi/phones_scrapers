# -*- coding: utf-8 -*-
from redis import Redis
from redis import ConnectionPool

from common.configuration import get_or_create
from common.configuration import DEFAULT_CONFIG

redis_conn               = Redis(connection_pool=ConnectionPool(host='localhost', port=6379, db=0))
base_site_url            = 'https://www.salt.ch'
site_start_url           = 'https://www.salt.ch/fr/cashback/'
products_tree_key        = 'salt_products_tree'
products_list_key        = 'salt_products_list'
export_data_file_name    = 'salt.csv'
export_history_file_name = 'salt_{}.csv'


get_headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36',
}

ftp_user             = get_or_create('salt_ftp_user'            , DEFAULT_CONFIG['RECOMERCE_FTP_USER'     ])
ftp_group            = get_or_create('salt_ftp_group'           , DEFAULT_CONFIG['RECOMERCE_FTP_GROUP'    ])
ftp_data_path        = get_or_create('salt_ftp_data_path'       , DEFAULT_CONFIG['RECOMERCE_FTP_DATA_PATH'])
processing_pool_size = get_or_create('salt_processing_pool_size', DEFAULT_CONFIG['PROCESSING_POOL_SIZE'   ])

ftp_history_path = get_or_create('salt_ftp_history_path', DEFAULT_CONFIG['RECOMERCE_FTP_HYSTORY_PATH'] + '/salt')
