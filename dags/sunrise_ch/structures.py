from collections import namedtuple

VariationCriteria  = namedtuple('VariationCriteria' , ['label'   , 'info'    ])
GradeSpecification = namedtuple('GradeSpecification', ['question', 'response'])

class ProductCriteria(namedtuple('ProductCriteria', ['price', 'label', 'prices', 'info'])):
    def __new__(cls, price, label, prices, info=None):
        return super(ProductCriteria, cls).__new__(cls, price, label, prices, info)
