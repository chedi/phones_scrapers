from __future__        import division

from csv               import DictWriter
from json              import loads as jloads
from pickle            import loads as ploads
from pickle            import dumps as pdumps
from hashlib           import sha1
from os.path           import join
from datetime          import datetime
from itertools         import product

from utils.url                import get_url
from utils.search             import SearchHelper
from utils.export             import setup_ftp_file
from utils.export             import normalize_price
from common.models            import Variable
from utils.processing         import multiprocessing_pool
from sunrise_ch.export        import CSV_DEFAULT_DATA
from sunrise_ch.export        import CSV_DATA_EXPORT_HEADERS
from sunrise_ch.structures    import ProductCriteria
from sunrise_ch.specification import GRADES
from sunrise_ch.specification import MODELS_VARIATIONS_CRITERIA
from sunrise_ch.configuration import redis_conn
from sunrise_ch.configuration import base_site_url
from sunrise_ch.configuration import products_list_key
from sunrise_ch.configuration import categories_list_key
from sunrise_ch.configuration import ftp_user
from sunrise_ch.configuration import ftp_group
from sunrise_ch.configuration import ftp_data_path
from sunrise_ch.configuration import ftp_history_path
from sunrise_ch.configuration import processing_pool_size


def get_articles(url):
    return {
        article.find('span').text : base_site_url + article.attrs['href']
        for article in SearchHelper(url).mc_find([('a', 'article', 'a')])}

def clear_redis_keys():
    for category_key in redis_conn.hgetall(categories_list_key):
        redis_conn.delete(category_key)
    redis_conn.delete(products_list_key  )
    redis_conn.delete(categories_list_key)

def explore_site_tree():
    categories = get_articles(base_site_url)
    redis_conn.hmset(categories_list_key, categories)
    for name, category_url in categories.iteritems():
        redis_conn.hmset(category_url, get_articles(category_url))

def get_product_information(brand_url, brand_name, product_name, product_url):
    calculator_data_url = filter(lambda _: _.startswith('/BuyCalculation'),
                                 SearchHelper(product_url).find_attr([('script', None, 'a')], 'src'))
    if calculator_data_url:
        calculator_data = jloads(get_url(base_site_url + calculator_data_url[0]).replace(
            'var purchaseCalculatorData = ', '').replace (';', ''))
        redis_conn.hset(products_list_key, product_url, pdumps({
            'brand'     : brand_name,
            'product'   : product_name,
            'created'   : datetime.now(),
            'curency'   : calculator_data['isoCurrencySymbol'],
            'base_price': calculator_data['basePrice'        ],
            'variations': {
                variation['translation']['fr']: map(
                    lambda _: ProductCriteria(_['price'], _['translation']['fr'], _['prices']),
                    variation['answers'])
                for variation in calculator_data['reductions']}}))

def scrap_products():
    with multiprocessing_pool(processes=processing_pool_size) as pool:
        for brand_name, brand_url in redis_conn.hgetall(categories_list_key).iteritems():
            for product_name, product_url in redis_conn.hgetall(brand_url).iteritems():
                pool.apply_async(
                    get_product_information,
                    (brand_url, brand_name, product_name, product_url))

def export_data():
    data = redis_conn.hgetall(products_list_key)
    if data:
        export_data_file    = join(ftp_data_path, 'sunrise_ch.csv')
        export_history_file = join(ftp_history_path, 'sunrise_ch_{}.csv'.format(datetime.now()))

        with open(export_history_file, 'w') as csv_file:
            writer = DictWriter(csv_file, fieldnames=CSV_DATA_EXPORT_HEADERS, delimiter=';')
            writer.writeheader()
            for url, raw_product in data.iteritems():
                row_data     = CSV_DEFAULT_DATA.copy()
                product_data = ploads(raw_product)

                for models_variations in product(*filter(bool, [[
                        ProductCriteria(variation.price, variation.label, variation.prices, criteria.label)
                        for variation in variations for criteria in criterias if question == criteria.info]
                    for criterias in MODELS_VARIATIONS_CRITERIA
                        for question, variations in product_data['variations'].iteritems()])):

                    model_specs     = ''
                    model_reduction = 0

                    for model_variation in models_variations:
                        model_specs     += "{}: {} - ".format(model_variation.info, model_variation.label)
                        model_reduction -= model_variation.price
                    model_specs = model_specs[:-2]

                    for grade, specifications in GRADES.iteritems():
                        price = base_price = product_data['base_price'] + model_reduction
                        for specification in specifications:
                            if specification.question in product_data['variations']:
                                question_price = filter(lambda _: _.label == specification.response,
                                                        product_data['variations'][specification.question])
                                if question_price:
                                    price -= round(base_price * max(
                                        filter(lambda _: _['border'] <= base_price,
                                               question_price[0].prices))['price'])
                                else:
                                    print((url, specification.question, product_data['variations'][specification.question]))
                        row_data.update({
                            'Url'      : url,
                            'Name'     : product_data['product'],
                            'Brand'    : product_data['brand'  ],
                            'Model'    : '',
                            'Price'    : normalize_price(price if price > 0 else 0),
                            'Storage'  : model_specs,
                            'Grading'  : grade,
                            'Timestamp': product_data['created'],
                        })
                        writer.writerow(row_data)
        setup_ftp_file(export_data_file, export_history_file, ftp_user, ftp_group)

def calculator_checking():
    data             = redis_conn.hgetall(categories_list_key)
    product_url      = next(redis_conn.hgetall(next(data.itervalues())).itervalues())
    saved_checksum   = Variable.get('sunrise_ch_calculator_checksum')
    calculator_url   = filter(lambda _: _.startswith('/bundles/purchase.calculator'),
                              SearchHelper(product_url).find_attr([('script', None, 'a')], 'src'))
    current_checksum = sha1(get_url(base_site_url + calculator_url[0], {})).hexdigest()

    if saved_checksum and (not calculator_url or saved_checksum != current_checksum):
        return 'alert'
    else:
        Variable.set('sunrise_ch_calculator_checksum', current_checksum)
        return 'scrap_products'

def alert_options():
    return {
        'to'          : Variable.get("ADMINS_EMAILS", deserialize_json=True),
        'subject'     : u'The code of the sunrise.ch purchase.calculator seems to have changed',
        'html_content': u'{{ ds }} : vendre.ch parshace.calculator have changed',
    }
