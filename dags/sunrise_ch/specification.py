# -*- coding: utf-8 -*-
from collections import OrderedDict

from vendre_ch.structures import VariationCriteria
from vendre_ch.structures import GradeSpecification


MODELS_VARIATIONS_CRITERIA = [
    [VariationCriteria('Memoire', u"M\xe9moire vive")],
    [VariationCriteria('Memoire', u"Quelle est la capacit\xe9/m\xe9moire de votre appareil ?"),
     VariationCriteria('Disque', u"M\xe9moire de la disque dur")]]

GRADES = OrderedDict([
    ('Très bon', [
        GradeSpecification(u"Quel est l’état optique de l‘appareil?"                    , u"Très bon"),
        GradeSpecification(u"Le chargeur (original) est-il joint à l'appareil?"         , u"Non"),
        GradeSpecification(u"Le câble du chargeur (original) est-il joint à l'appareil?", u"Non"),
        GradeSpecification(u"Le verre tactile fonctionne-t-il sans problème ?"          , u"Oui"),
        GradeSpecification(u"Est-ce que toutes les touches fonctionnent ?"              , u"Oui"),
        GradeSpecification(u"Est-ce que l’appareil a été acheté en Suisse?"             , u"Oui")]),
    ('Utilisé', [
        GradeSpecification(u"Quel est l’état optique de l‘appareil?"                    , u"Utilisé"),
        GradeSpecification(u"Le chargeur (original) est-il joint à l'appareil?"         , u"Non"),
        GradeSpecification(u"Le câble du chargeur (original) est-il joint à l'appareil?", u"Non"),
        GradeSpecification(u"Le verre tactile fonctionne-t-il sans problème ?"          , u"Oui"),
        GradeSpecification(u"Est-ce que toutes les touches fonctionnent ?"              , u"Oui"),
        GradeSpecification(u"Est-ce que l’appareil a été acheté en Suisse?"             , u"Oui")]),
    ('Passable', [
        GradeSpecification(u"Quel est l’état optique de l‘appareil?"                    , u"Passable"),
        GradeSpecification(u"Le chargeur (original) est-il joint à l'appareil?"         , u"Non"),
        GradeSpecification(u"Le câble du chargeur (original) est-il joint à l'appareil?", u"Non"),
        GradeSpecification(u"Le verre tactile fonctionne-t-il sans problème ?"          , u"Oui"),
        GradeSpecification(u"Est-ce que toutes les touches fonctionnent ?"              , u"Oui"),
        GradeSpecification(u"Est-ce que l’appareil a été acheté en Suisse?"             , u"Oui")]),
    ('Ecran cassé', [
        GradeSpecification(u"Quel est l’état optique de l‘appareil?"                    , u"Utilisé"),
        GradeSpecification(u"Le chargeur (original) est-il joint à l'appareil?"         , u"Non"),
        GradeSpecification(u"Le câble du chargeur (original) est-il joint à l'appareil?", u"Non"),
        GradeSpecification(u"Le verre tactile fonctionne-t-il sans problème ?"          , u"Non"),
        GradeSpecification(u"Est-ce que toutes les touches fonctionnent ?"              , u"Oui"),
        GradeSpecification(u"Est-ce que l’appareil a été acheté en Suisse?"             , u"Oui")]),
    ])
