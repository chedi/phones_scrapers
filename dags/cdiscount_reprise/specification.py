GRADING_MAP = {
    'Etat 1': {
        'condition_q1'  : 'yes',
        'condition_q2'  : 'yes',
        'condition_q3'  : 'yes',
        'condition_q4'  : 'yes',
        'condition_qios': 'no' ,
    },
    'Etat 2': {
        'condition_q1'  : 'yes',
        'condition_q2'  : 'yes',
        'condition_q3'  : 'no' ,
        'condition_q4'  : 'yes',
        'condition_qios': 'no' ,
    },
    'Etat 3': {
        'condition_q1'  : 'no' ,
        'condition_q2'  : 'yes',
        'condition_q3'  : 'yes',
        'condition_q4'  : 'yes',
        'condition_qios': 'no' ,
    },
    'Etat 4': {
        'condition_q1'  : 'no' ,
        'condition_q2'  : 'yes',
        'condition_q3'  : 'no' ,
        'condition_q4'  : 'yes',
        'condition_qios': 'no' ,
    },
    'Etat 5': {
        'condition_q1'  : 'no' ,
        'condition_q2'  : 'no' ,
        'condition_q3'  : 'no' ,
        'condition_q4'  : 'yes',
        'condition_qios': 'no' ,
}}
