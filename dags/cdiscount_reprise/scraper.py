from json            import loads as jloads
from pickle          import dumps as pdumps
from pickle          import loads as ploads
from urllib          import quote
from os.path         import join
from datetime        import datetime
from unicodecsv      import DictWriter
from urllib3.request import urlencode

from utils.url                       import post_url
from utils.export                    import setup_ftp_file
from utils.export                    import normalize_price
from utils.search                    import SearchHelper
from utils.common                    import merge_dicts
from utils.processing                import multiprocessing_pool
from cdiscount_reprise.export        import CSV_DEFAULT_DATA
from cdiscount_reprise.export        import CSV_DATA_EXPORT_HEADERS
from cdiscount_reprise.specification import GRADING_MAP
from cdiscount_reprise.configuration import ftp_user
from cdiscount_reprise.configuration import ftp_group
from cdiscount_reprise.configuration import redis_conn
from cdiscount_reprise.configuration import post_headers
from cdiscount_reprise.configuration import ftp_data_path
from cdiscount_reprise.configuration import base_site_url
from cdiscount_reprise.configuration import price_ws_url
from cdiscount_reprise.configuration import brands_ws_url
from cdiscount_reprise.configuration import models_ws_url
from cdiscount_reprise.configuration import product_id_regex
from cdiscount_reprise.configuration import ftp_history_path
from cdiscount_reprise.configuration import brands_ws_headers
from cdiscount_reprise.configuration import products_list_key
from cdiscount_reprise.configuration import products_tree_key
from cdiscount_reprise.configuration import brands_ws_json_body
from cdiscount_reprise.configuration import processing_pool_size

def clear_redis_keys():
    for product_key in redis_conn.hgetall(products_list_key):
        redis_conn.delete(product_key)
    for product_key in redis_conn.hgetall(products_tree_key):
        redis_conn.delete(product_key)
    redis_conn.delete(products_tree_key)
    redis_conn.delete(products_list_key)

def get_brand_models(brand_name):
    page     = SearchHelper(models_ws_url.format(quote(brand_name)))
    products = page.mc_find([('div', 'item-product', 'a')])

    if products:
        redis_conn.hset(products_tree_key, brand_name, pdumps([(
            product.find('a').attrs['href'], product.find(class_='item-name').text)
            for product in products]))

def explore_site_tree():
    brands_data = jloads(post_url(brands_ws_url, brands_ws_headers, brands_ws_json_body))
    with multiprocessing_pool(processes=processing_pool_size) as pool:
        if brands_data['CategoryGroupList'] and brands_data['CategoryGroupList'][1]:
            for brand in brands_data['CategoryGroupList'][1]['CategoryList']:
                pool.apply_async(get_brand_models, (brand['DisplayName'],))

def scrap_products_worker(brand_name, product_name, product_url, grade_name, grade_specification):
    req  = []
    url  = base_site_url + product_url
    page = SearchHelper(url, post_url(url, {}, None, req))
    for script in filter(bool, [_.string for _ in page.mc_find([('script', None, 'a')])]):
        search_result = product_id_regex.search(script)
        if search_result:
            product_id = search_result.group(1)
            break
    else:
        return

    params = merge_dicts(grade_specification, {'idproduct': product_id, 'action': 'amount'})
    redis_conn.hset(
        products_list_key,
        u'{}-{}'.format(product_url, grade_name), pdumps({
            'Url'      : url,
            'Name'     : product_name,
            'Price'    : normalize_price(jloads(post_url(price_ws_url, post_headers, urlencode(params)))['amount']),
            'Brand'    : brand_name,
            'Model'    : '',
            'Grading'  : grade_name,
            'Timestamp': datetime.now(),
        }))

def scrap_products():
    with multiprocessing_pool(processes=processing_pool_size) as pool:
        for brand_name, raw_products_urls in redis_conn.hgetall(products_tree_key).iteritems():
            products_urls = ploads(raw_products_urls)
            for product_url, product_name in products_urls:
                for grade_name, grade_specification in GRADING_MAP.iteritems():
                    pool.apply_async(scrap_products_worker,
                                     (brand_name, product_name, product_url, grade_name, grade_specification))

def export_data():
    data = redis_conn.hgetall(products_list_key)
    if data:
        export_data_file    = join(ftp_data_path, 'cdiscount_reprise.csv')
        export_history_file = join(ftp_history_path, 'cdiscount_reprise_{}.csv'.format(datetime.now()))

        with open(export_history_file, 'w') as csv_file:
            writer = DictWriter(csv_file, fieldnames=CSV_DATA_EXPORT_HEADERS, delimiter=';')
            writer.writeheader()
            for url, raw_product in data.iteritems():
                row_data     = CSV_DEFAULT_DATA.copy()
                product_data = ploads(raw_product)
                row_data.update(product_data)
                writer.writerow(row_data)
        setup_ftp_file(export_data_file, export_history_file, ftp_user, ftp_group)
