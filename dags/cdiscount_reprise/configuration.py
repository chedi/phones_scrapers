from re    import compile
from redis import Redis
from redis import ConnectionPool

from common.configuration import get_or_create
from common.configuration import DEFAULT_CONFIG

redis_conn        = Redis(connection_pool=ConnectionPool(host='localhost', port=6379, db=0))
post_headers      = {
    'Accept'          : 'application/json',
    'Content-Type'    : 'application/x-www-form-urlencoded',
    'X-Requested-With': 'XMLHttpRequest'
}
product_id_regex  = compile(r'idproduct=(\d+)')
products_tree_key = 'cdiscount_reprise_products_tree'
products_list_key = 'cdiscount_reprise_products_list'

price_ws_url        = 'http://recyclage-mobile.cdiscount.com/services/get_best_price.php'
base_site_url       = 'http://recyclage-mobile.cdiscount.com'
brands_ws_url       = 'http://orchestration.cdiscount.com/202/v1.0/GetWall.svc/rest/GetWall'
models_ws_url       = 'http://recyclage-mobile.cdiscount.com/recherche.php?search={}'
brands_ws_headers   = {'Content-Type': 'application/json', 'Accept': 'application/json'}
brands_ws_json_body = """
{"HeaderMessage":{
    "ApplicationInformations":{
        "Version":{"Major":3,"Minor":0,"Revision":3},
         "OsName":"Android","Id":202},
    "Country":"Fr","Currency":"Eur","Language":"Fr",
    "Tracking":{"TrackingPageEventList":null,"PageType":"000X123","SessionId":"","UserId":""}},
    "WallRequest":{"FilterCategoryList":null,"IncludeMarketPlace":true,"NodeId":66240,
    "Pagination":{"CurrentPageNumber":0,"ItemPerPage":12,"TotalPageCount":0}
}}"""

ftp_user             = get_or_create('cdiscount_reprise_ftp_user'            , DEFAULT_CONFIG['RECOMERCE_FTP_USER'     ])
ftp_group            = get_or_create('cdiscount_reprise_ftp_group'           , DEFAULT_CONFIG['RECOMERCE_FTP_GROUP'    ])
ftp_data_path        = get_or_create('cdiscount_reprise_ftp_data_path'       , DEFAULT_CONFIG['RECOMERCE_FTP_DATA_PATH'])
processing_pool_size = get_or_create('cdiscount_reprise_processing_pool_size', DEFAULT_CONFIG['PROCESSING_POOL_SIZE'   ])

ftp_history_path = get_or_create('cdiscount_reprise_ftp_history_path', DEFAULT_CONFIG['RECOMERCE_FTP_HYSTORY_PATH'] + '/cdiscount_reprise')
